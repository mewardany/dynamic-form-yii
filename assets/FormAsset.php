<?php
namespace wardany\dform\assets ;

use yii\web\AssetBundle;

/**
 *
 *
 * @author muhammad wardany
 */
class FormAsset extends AssetBundle{
    public $sourcePath = '@wardany/dform/web';
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $css = [
        'css/style.css',
        'css/loader.css',
    ];
    public $js = [
        // 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        'js/bootbox.min.js',
        'js/base_form.js',
        'js/form_builder.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
