<?php
namespace wardany\dform\assets ;

use yii\web\AssetBundle;

/**
 *
 *
 * @author muhammad wardany
 */
class IconPicker extends AssetBundle{
    public $sourcePath = '@wardany/dform/web/fontIconPicker';
    public $css = [
        'css/jquery.fonticonpicker.min.css',
        'themes/bootstrap-theme/jquery.fonticonpicker.grey.minr.css',
        'demo/fontello-7275ca86/css/fontello.css',
        'demo/icomoon/icomoon.css',
    ];
    public $js = [
        'icons.js',
        'jquery.fonticonpicker.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
