<?php

namespace wardany\dform\assets;

use dominus77\iconpicker\assets\FontAwesomeAsset;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class IconAsset extends AssetBundle
{
    public $sourcePath = '@wardany/dform/web/bootstrap-iconpicker-1.7.0';

    public $css = [
        'icon-fonts/font-awesome-4.2.0/css/font-awesome.min.css',
        'bootstrap-iconpicker/css/bootstrap-iconpicker.min.css',
    ];
    public $js = [
        'bootstrap-iconpicker/js/iconset/iconset-fontawesome-4.2.0.min.js',
        'bootstrap-iconpicker/js/bootstrap-iconpicker.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
