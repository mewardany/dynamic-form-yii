dynamic form generator
======================
this package generate dynamic form with client and server side validation

Installation
------------

This pugin is under development so download it manually.




Usage
-----

- copy plugin to the project directory,
- edit vendor/yiisoft/extensions.php file by adding this block before closing the file :

```php
'dform' =>
    array (
      'name' => 'wardany/dform',
      'version' => '1.0',
      'alias' =>
      array (
        '@wardany/dform' => '@frontend/../dform',
      ),
),
```

in config :

```php
'modules' => [
      ...
      'w_forms'=>[
            'class'=> "wardany\dform\DynamicForm",
            'upload_path'=> '@frontend/web/media/',
            'upload_url'=> 'media/'
        ],
        ...
]
```

Migrate
```php
php yii migrate --migrationPath=@wardany/dform/migrations
```

url: virtualhost.dev/w_forms/default
