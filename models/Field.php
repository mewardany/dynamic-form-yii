<?php

namespace wardany\dform\models;

use Yii;
use common\models\MainModel;

use wardany\dform\behaviors\SearchFormField;
use wardany\dform\behaviors\DynamicFormField;
use wardany\dform\behaviors\SluggableBehavior;
use wardany\dform\helpers\FieldHelper;
use yii\base\UnknownPropertyException;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "form_field".
 *
 * @property integer $id
 * @property integer $form_id
 * @property integer $field_id
 * @property string $field_type
 * @property string $attribute_name
 * @property string $attribute_label //
 * @property string $view_title
 * @property string $field_options
 * @property string $view_options
 * @property string $field_as_json
 * @property string $field_as_html
 * @property string $view_as_json
 * @property string $view_as_html
 * @property string $server_validations
 * @property string $client_validations
 * @property string $form_options
 * @property string $search_options
 *
 * @property Form $form
 * @property PostDetails[] $postDetails
 */
class Field extends MainModel
{
    public $translation_category = 'dform_labels';
    protected $_field_options = [];
    public $_form_options =[
        'parent_node_id'=> null,
        'order'=> 0,
        'hide_label' => false,
        'options'=>['class' => 'form-group'],
        'inputOptions'=> ['class' => 'form-control'],
        'errorOptions'=> ['class' => 'help-block'],
        'labelOptions'=> [
            'class' => 'control-label'
        ],
        'hintOptions'=> ['class' => 'hint-block']
    ];
    public $_search_options =[
            'allow_search'=> false,
            'search_widget'=> null
    ];



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_id', 'field_id'], 'integer'],
            [['attribute_label'], 'required'],
            ['attribute_label', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z_ -]/',],
            [['attribute_name'], 'match', 'not' => true, 'pattern' => '/[^a-zA-Z_]/','message' => Yii::t('app', 'Invalid characters')],
            [['attribute_name', 'attribute_label', 'field_options', 'form_options', 'view_options'], 'string'],
            [['attribute_name'], 'unique', 'targetAttribute' => ['attribute_name', 'form_id']],
            [['field_type'], 'string', 'max' => 255],
            [['field_type', 'searchWidget'], 'in', 'range'=> array_keys(FieldHelper::fields())],
            ['formOptionsClass', 'string', 'max'=> 255],
            ['allowSearch', 'boolean'],
            ['searchWidget', 'string'],
        ];
    }

    public function scenarios() {
        return [
            'insert'=>['attribute_label', 'attribute_name', 'formOrder', 'formParentNodeId', 'searchWidget', 'allowSearch'],
            'update'=>['attribute_label', 'formOrder', 'formParentNodeId', 'searchWidget', 'allowSearch'],
            'auto-insert'=>['attribute_label', 'attribute_name', 'field_options', 'form_id', 'field_type', 'form_options', 'search_options', 'searchWidget', 'allowSearch'],
            'auto-update'=>['attribute_label', 'field_options', 'form_options', 'search_options', 'searchWidget', 'allowSearch'],
            'sort'=> ['form_options', 'formOrder', 'formParentNodeId']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'form_id' => 'Field ID',
            'field_id' => 'Field ID',
            'field_type' => 'Field Type',
            'attribute_name' => 'Attribute Name',
            'attribute_label' => 'Attribute Label',
            'view_title' => 'View Title',
            'field_options' => 'Field Options',
            'form_options' => 'Form Options',
            'search_options' => 'Search Options',
            'view_options' => 'View Options',
            'field_as_json' => 'Field As Json',
            'field_as_html' => 'Field As Html',
            'view_as_json' => 'View As Json',
            'view_as_html' => 'View As Html',
        ];
    }

    public function behaviors() {
        return [
            [
                'class'=> SluggableBehavior::className(),
                'attribute'=>'attribute_label',
                'slugAttribute'=>'attribute_name',
                'ensureUnique'=> true,
                'immutable'=> true
            ],
            [
                'class'=> DynamicFormField::className(),
            ],
            [
                'class'=> SearchFormField::className(),
            ],
        ];
    }

    public function afterFind() {
        parent::afterFind();
        $this->_field_options = $this->getArrayFromJson($this->field_options);
        $this->_form_options = $this->getArrayFromJson($this->form_options);
    }

    public function beforeValidate(){
        $this->field_options = Json::encode($this->_field_options);
        $this->form_options = Json::encode($this->_form_options);
        $this->search_options = Json::encode($this->_search_options);
        return parent::beforeValidate();
    }

    public function getArrayFromJson($json){
        if(!empty($json)){
            if(is_array(Json::decode($json)))
                return Json::decode($json);
        }
        else
            return [];
    }


    // public function beforeSave($insert) {
    //
    //     $this->field_options = Json::encode($this->_field_options);
    //     $this->form_options = Json::encode($this->_form_options);
    //     return parent::beforeSave($insert);
    // }


    public function afterSave($insert, $changedAttributes) {
        if (@$this->_form_options['place_holder']) {
          $this->addTranslation($this->translation_category, $this->_form_options['place_holder']);
        }
        if (@$this->_form_options['prompt']) {
          $this->addTranslation($this->translation_category, $this->_form_options['prompt']);
        }
        $this->addTranslation($this->translation_category, $this->attribute_label);
        $this->form->createRules();
        parent::afterSave($insert, $changedAttributes);
    }

    public function getInput($model = null, $form = null){
        $field_class = FieldHelper::field($this->field_type)['class'];
        $field = $field_class::findOne($this->id);

        if(!$model && !$form)
            return $field->getInput($model, $form);

        $form->fieldConfig = \yii\helpers\ArrayHelper::merge($form->fieldConfig, ['field'=> $field]);

        // ddd($model->extra_attributes);

        switch ($field->field_type) {
            case FieldHelper::CHECKBOX_LIST:
                return $form->field($model, $this->attribute_name)->dynamicCheckboxList() ;
            case FieldHelper::DROPDOWN_LIST:
                return $form->field($model, $this->attribute_name)->dynamicDropDownList() ;
            case FieldHelper::RADIO_LIST:
                return $form->field($model, $this->attribute_name)->dynamicRadioList() ;
            case FieldHelper::TEXT_AREA:
                return $form->field($model, $this->attribute_name)->dynamicTextarea() ;
            default:
                return $form->field($model, $this->attribute_name)->dynamicTextInput(['maxlength' => true]) ;
        }
    }

    public function getSearchInput($model, $form){
        $field_class = FieldHelper::field($this->field_type)['class'];
        $field = $field_class::findOne($this->id);

        $form->fieldConfig = \yii\helpers\ArrayHelper::merge($form->fieldConfig, ['field'=> $field]);

        $field_type = empty($field->searchWidget)? $field->field_type: $field->searchWidget;

        switch ($field_type) {
            case FieldHelper::CHECKBOX_LIST:
                return $form->field($model, $this->attribute_name)->dynamicCheckboxList() ;
            case FieldHelper::DROPDOWN_LIST:
                return $form->field($model, $this->attribute_name)->dynamicDropDownList() ;
            case FieldHelper::RADIO_LIST:
                return $form->field($model, $this->attribute_name)->dynamicRadioList() ;
            case FieldHelper::TEXT_AREA:
                return $form->field($model, $this->attribute_name)->dynamicTextarea() ;
            default:
                return $form->field($model, $this->attribute_name)->dynamicTextInput(['maxlength' => true]) ;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'form_id'])->inverseOf('fields');
    }


    public function getOption($option) {
        try {
            return $this->{$option};
        } catch (UnknownPropertyException $exc) {
            return null;
        }
    }

    public function getRules() {
        $field = FieldHelper::field($this->field_type)['class'];
        $model = $field::findOne($this->id);
        return $model->getRules();
    }

    public static function htmlElements($element = null){
        $elements = [
            'formParentNodeId'      => 'Parent id',
            'formOrder'             => 'Order',
            'formTemplate'          => 'Template',
            'hideLabel'             => 'Hide label',
            'formOptionsClass'      => 'Container class',
            'formInputOptionsClass' => 'Input class',
            'formErrorOptionsClass' => 'Error class',
            'formLabelOptionsClass' => 'Label class',
            'formHintOptionsClass'  => 'Hint class',
        ];
        if($element === null)
            return $elements;
        else
            return $elements[$element];
    }

    public static function getFieldIdByName($name, $category)
    {
        $condition = ['attribute_name'=>$name];
        // if($category && $category->form){
        //     $condition['form_id'] = $category->form_id;
        // }
        $field = Field::find()->select(['id'])->where($condition)->one();
        return $field? $field->id: false;
    }

    public static function getNameById($field_id)
    {
      return Field::find()->where(['id'=>$field_id])->one();
    }

    public function getTLabel()
    {
        return Yii::t($this->translation_category, $this->attribute_label);
    }

    public function getTItem($item)
    {
        return Yii::t($this->translation_category, $item);
    }

    public function translateItems(array $items)
    {
      foreach ($items as $key => $item) {
        $this->addTranslation($this->translation_category, $item);
      }
    }

}
