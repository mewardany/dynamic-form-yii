<?php

namespace wardany\dform\models;

use Yii;
use wardany\dform\behaviors\DynamicFormBehavior;
use yii\helpers\Json;

/**
 * This is the model class for table "form".
 *
 * @property integer $id
 * @property string $form_options
 * @property string $view_options
 * @property string $server_validations
 * @property string $client_validations
 * @property string $title
 *
 * @property Field[] $fields
 */
class Form extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_options', 'view_options', 'server_validations', 'client_validations'], 'string'],
            [['form_options', 'view_options', 'server_validations', 'client_validations'], 'default', 'value'=> Json::encode(array())],

            ['title', 'string', 'max'=> 255],
            ['title', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'form_options' => 'Form Options',
            'view_options' => 'View Options',
            'server_validations' => 'Server Validation',
            'client_validations' => 'Client Validation',
            'Title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(Field::className(), ['form_id' => 'id']);
    }

    public function createRules() {
        $fields = Field::find()->where(['form_id'=> $this->id])->all();
        $rules = [];
        foreach ($fields as $field) {
            foreach ($field->getRules() as $rule) {
                $rules[]= $rule ;
            };
        }
        $this->server_validations = Json::encode($rules);
        $this->save();
    }
    
    public function beforeDelete() {
        foreach ($this->fields as $field) {
            $field->delete();
        }
        return parent::beforeDelete();
    }
}
