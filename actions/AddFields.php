<?php
namespace wardany\dform\actions;

use Yii;

/**
 * Description of AddFields
 *
 * @author muhammad wardany
 */

class AddFields extends \yii\base\Action{

    public $view_file = '@wardany/dform/views/fields/index';
    public $model ;
    
    public function run(int $id)
    {
        return $this->controller->render($this->view_file, ['model'=>  $this->getModel($id)]);
    }
    
    private function getModel($id) {
        $class = $this->model ;
        if (($model = $class::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
