<?php
namespace wardany\dform\actions;

use Yii;
use yii\web\NotFoundHttpException;
use wardany\dform\helpers\FieldHelper;

/**
 * Description of AddFields
 *
 * @author muhammad wardany
 */

class AddField extends \yii\base\Action{

    public $view_path = '@wardany/dform/views/fields';

    public function run(int $form_id, string $field ){
        if(!FieldHelper::field($field))
            throw new NotFoundHttpException('The requested page does not exist.');
        $field_options = FieldHelper::field($field);
        $base_field = new \wardany\dform\models\Field();
        $input_field = new $field_options['class'] ;

        $base_field->form_id = $form_id;
        $base_field->field_type = $field;
        if($this->save($base_field, $input_field, Yii::$app->request->post(), $field))
        {
            return $base_field->getInput();
        }
            
        $render_option = Yii::$app->request->isAjax? 'renderAjax': 'render';
        return $this->controller->$render_option($this->view_path.'/'.$field_options['form'],['baseField'=> $base_field, 'model'=> $input_field]);
    }

    /**
     * 
     * @param \wardany\dform\models\Field $base_model
     * @param \yii\web\Request $request
     */
    public function save($base_model, $input_model, $request, $relation) {
        if ($base_model->load($request) && $base_model->save()) {
            if ($input_model->load($request) && $input_model->save()) {
                $base_model->link($relation, $input_model);
            }
            return true;
        }
        return false;
    }
    
}
