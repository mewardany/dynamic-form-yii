<?php

namespace wardany\dform\controllers;

use wardany\dform\searchmodels\FormSearch;
use wardany\dform\helpers\FieldHelper;
use wardany\dform\models\Field;
use wardany\dform\models\Form;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * DefaultController implements the CRUD actions for Form model.
 */
class DefaultController extends Controller
{

    public function init() {
        $this->setViewPath('@wardany/dform/views/');
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'remove-field' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Form models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('forms/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Form model.
     * @param int $id
     * @return mixed
     */
    public function actionView($id){
        $this->findModel($id)->createRules();

        return $this->render('forms/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Form model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Form();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-fields', 'id' => $model->id]);
        } else {
            return $this->render('forms/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Form model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('forms/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Form model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Start building form and adding fields.
     * @param int $id
     * @return mixed
     */
    public function actionAddFields($id){
        return $this->render('fields/index', ['model'=>  $this->findModel($id)]);
    }

    /**
     * add single field to the form
     * field name must follow naming protocol
     *
     * @param int $id
     * @param string $field
     * @return mixed
     */
    public function actionAddField( $id, $field ){
        if(!Yii::$app->request->isAjax)
            throw new NotFoundHttpException('The requested page does not exist.');
        $model = $this->findField($field);
        $model->setScenario('insert');
        if (Yii::$app->request->post('parent_node_id') && $model->load(Yii::$app->request->post()) && ($model->form_id = $id) && ($model->field_type = $field)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            // $model->formOrder = Yii::$app->request->post('order') ;
            // $model->formParentNodeId = Yii::$app->request->post('parent_node_id') ;
            if($model->save()){
                return ['success'=> true];
            }
            else
                return ['success'=> false, 'errors'=> \yii\helpers\Html::errorSummary($model)];
        }
        else
            return $this->renderAjax('fields/_form',['model'=> $model, 'extras_form'=> FieldHelper::field($field)['form'], 'form_id'=>$id, 'field'=>$field]);
    }

    /**
     * edit single field
     *
     * @param int $id
     * @return Field
     */
    public function actionEditField($id){
        if(!Yii::$app->request->isAjax)
            throw new NotFoundHttpException('The requested page does not exist.');
        $model = $this->findFieldById($id);
        $model->setScenario('update');
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->setScenario('auto-update');
            return $model->save()? ['success'=> true]: ['success'=> false, 'errors'=> \yii\helpers\Html::errorSummary($model)];
        }
        else
            return $this->renderAjax('fields/_form',['model'=> $model, 'extras_form'=> FieldHelper::field($model->field_type)['form']]);
    }

    /**
     * edit single field
     *
     * @param int $id
     * @return Field
     */
    public function actionRemoveField($id)
    {
        $model = $this->findFieldById($id);
        $form = $this->findModel($model->form_id);
        $model->delete();
        return Yii::$app->request->isAjax ? \wardany\dform\widgets\Titles::widget(['model'=> $form]) : $this->redirect(['add-fields', 'id'=>$form->id]);
    }

    /**
     * edit single field
     *
     * @param int $id
     */
    public function actionSortFields()
    {
        if(!Yii::$app->request->isAjax)
            throw new NotFoundHttpException('The requested page does not exist.');

        $fields = Yii::$app->request->post('fields');
        if(!$fields && !is_array($fields))
            throw new NotFoundHttpException('The requested page does not exist.');

        foreach($fields as $index => $field_id){
            $field = $this->findFieldById($field_id);
            $field->setScenario('sort');
            $field->formOrder = $index+1 ;
            $field->save();
        }
    }



    /**
     * Finds the Form model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return Form the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Form::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Field model based on its field name.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $field
     * @return Field the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findField($field)
    {
        if(!FieldHelper::field($field))
            throw new NotFoundHttpException('The requested page does not exist.');

        $field_options = FieldHelper::field($field);
        return new $field_options['class'] ;
    }


    /**
     * Finds the Field model based on its id.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return Field the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFieldById($id)
    {
        if (($field = Field::findOne($id)) !== null) {
            if(!FieldHelper::field($field->field_type))
                throw new NotFoundHttpException('The requested page does not exist.');
            $field_class = FieldHelper::field($field->field_type)['class'];
            return $field_class::findOne($id);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
