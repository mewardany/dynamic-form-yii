/**
 * options =[form_options]
 * attributes=[
 *  field_id=[
 *      options =       ['class' => 'form-group']
 *      template =      "{label}\n{input}\n{hint}\n{error}"
 *      inputOptions =  ['class' => 'form-control']
 *      errorOptions =  ['class' => 'help-block']
 *      labelOptions =  ['class' => 'control-label']
 *      hintOptions =   ['class' => 'hint-block']
 *  ]
 *  ...
 * ] 
 */

var draggable_class     = "draggable";
var disabled_class      = "disable_over";
var form_id             = 'dynamic_form';

var container_default_class = 'form-group';
var label_default_class = 'control-label';
var input_default_class = 'form-control';
var error_block_default_class = 'help-block';
var hint_block_default_class = 'hint-block';
var form_modal = 'dynamic-form-modal';

var clicked ;
var modal   =  $('.'+form_modal);
var form_options   = [];

$( document).ready(function() {
    init();
});

function init(){
    
    jQuery.each( $('*[field_id]'), function( i, field ) {
        $(field).closest('div').addClass('container-field');
        $(field).prev('label').addClass('error-field');
        $(field).addClass('input-field');
        $(field).next('.'+error_block_default_class).addClass('error-field');
        $(field).next('.'+hint_block_default_class).addClass('hint-field');
    });    
    
//    $('.'+container_default_class).addClass('container-field');
//    $('.'+label_default_class).addClass('label-field');
//    $('.'+error_block_default_class).addClass('error-field');
//    $('.'+hint_block_default_class).addClass('');
    
    form_options = getOptions('#'+form_id+' div');
    makeSortable();
}

$(document).on('click', '.'+draggable_class, function(e){
    
    var container =  $(e.target).parent();
    var label =  container.find('label');
    var input =  container.find('.'+input_default_class);
    var error_block =  container.find('.'+error_block_default_class);
    var hint_block =  container.find('.'+hint_block_default_class);
    
    clicked = container ;
        
    modal.find('.modal-header h2').html(container.find('label').text()); // modal title
    
    modal.find('#container-class').val(container.attr('c-width')); // container class
    modal.find('#container-class').val(container.attr('c-break')); // container class
    modal.find('#container-class').val(container.attr('c-class')); // container class
    modal.find('#container-style').val(container.attr('c-style')); // container class
    
    modal.find('#label-class').val(label.attr('c-class')); // input class
    modal.find('#label-style').val(label.attr('c-style')); // input class
    
    modal.find('#input-class').val(input.attr('c-class')); // input class
    modal.find('#input-style').val(input.attr('c-style')); // input class
    
    modal.find('#error-class').val(error_block.attr('c-class')); // error class
    modal.find('#error-style').val(error_block.attr('c-style')); // error class
    
    modal.find('#hint-class').val(hint_block.attr('c-class')); // hint class
    modal.find('#hint-style').val(hint_block.attr('style')); // hint class
    
   modal.modal('show');
});

$(document).on('submit', '#dynamic-form-modal', function(){
    saveModal($(this).serializeArray());
    return false ;
});
$(document).on('click', '#save-dynamic-modal-form', function(){
    $('#dynamic-form-modal').submit();
    return false;
});

function saveModal(data){
    jQuery.each( data, function( i, field ) {
        name = field.name ;
        class_name = name.substring(0, name.indexOf('-c-'));
        attr_name = name.substring(name.indexOf('-c-')+1, name.length);
        if($(clicked).hasClass(class_name)){
            $(clicked).attr(attr_name, field.value) ;
        }
        else if($(clicked).find('.'+class_name) != undefined){
            $(clicked).find('.'+class_name).attr(attr_name, field.value);
        }
    });
    modal.modal('show');
}


// functions

/**
 * 
 * @param selector element
 * @returns JSON
 */
function getOptions(element){
    // get all div's inside form
    
    return {
        formOptions : getAttributes($(element)),
        fields      : getFields($(element))
    };
}

function getFields(element){
    var fields = {};
    element.each(function(index) {
        var ID = $(this).attr('id');

        if($("#" + ID).length !== 0 && ID.match("^field")) {
            $(this).addClass(draggable_class);
            $(this).prepend('<div class="'+disabled_class+'"></div>');
            // collect attributes
            fields[index] = {
                options: getFieldsOptions($(this)),
                inputOptions :  getInputOptions($(this)),
                labelOptions :  getLabelOptions($(this)),
            };
        }
    });
    return fields ;
}

function getFieldsOptions(e){
    return getAttributes(e);
}

function getInputOptions(e){
    var input = e.find('#'+e.attr('id').replace('field-', ''));
    if(input.length)
        return getAttributes(input);
    return {};
}

function getLabelOptions(e){
    var label = e.find('label');
    if(label.length)
        return getAttributes(label);
    return {};
}

function getErrorOptions(e){
    var error = e.find('.'+error_block_default_class);
    if(error.length)
        return getAttributes(error);
    return {};
}

function getHintOptions(){
    var hint = e.find('.'+hint_block_default_class);
    if(hint.length)
        return getAttributes(hint);
    return {};
    
}

function getAttributes ( $node ) {
    var attrs = {};
    $.each( $node[0].attributes, function ( index, attribute ) {
        attrs[attribute.name] = attribute.value;
    } );

    return attrs;
}

function makeSortable(){
    $('#'+form_id).sortable({
      revert: true
    });
}

function makeDrag(){
    $('.'+draggable_class).draggable({ 
        snap: true,
        connectToSortable: '#'+form_id,
        helper: "clone",
        revert: "invalid"
    });
    $( "#"+form_id+" , div" ).disableSelection();
}

function removeBaseOnArray(str, arr){
    
    for(i=0; i < arr.length; i++){
        str = str.replace(arr[i],'');
    }
    return str;
}