/*
    Created on : Feb 13, 2017, 3:33:22 PM
    Author     : muhammad wardany
*/

var form = $('#form-data');

jQuery(document).ready(function(){
   // makeSortable();
   // makeDraggable();
//    sort();
});

$(document).ajaxSuccess(function() {
//    makeSortable();
//    sort();
});



function makeSortable(){
    $(".fields-list").sortable({
        connectWith: '.sortable-field',
        opacity: 0.8,
        cursor: 'move',
        update: function() {
            // jQuery.each( $('#inputs-list [name]'), function( i, field ) {
            //     console.log($(field).attr('name'));
            // });
        }
    });
}

function makeDraggable(){
    $( ".sortable-field" ).draggable({
         appendTo: ".droppable",
        //  helper: "clone",
         refreshPositions: true
     });
    $( ".droppable" ).droppable({
      drop: function( event, ui ) {
          console.log($(event.target).attr('class'));
          $(event.target).append(ui.draggable);
          $(ui.draggable).removeAttr('style');
          return false;
      }
    });
}

function sort(){
    $('.fields-list>div').sort(function(a,b){
        return a.dataset.sid > b.dataset.sid
     }).appendTo('.fields-list')
}
