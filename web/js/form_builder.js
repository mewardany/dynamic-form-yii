
var drop_area ;
var modal;

$( "#sortable" ).sortable({
    cursor: "move",
    update: function(event, ui) {
            var fields_ids = $(this).sortable('toArray', {attribute: 'data-id'});
            sortElements($(this).attr('sort-url'), fields_ids);
    }
});
$( "#sortable" ).disableSelection();

function sortElements(url, data){
    $.ajax({
        url: url,
        type: 'post',
        data: {fields: data},
        success: function () {
            return true;
        },
        error: function(xhr){
            return false;
        }
    });
}


jQuery(document).on('click', '.get-field', function(e){
    var url = $(this).data('href');
    var title = $(this).attr('title');
    showModal(url, title);
});

function showModal(url, title){
   $.ajax({
        url: url,
        success: function(data){
            $('html body').append(data);
            $('#form-title').html(title);
            modal = $('#add-field-modal');
            modal.find('.modal-content').prepend('<div class="dimm"><i class="loader"></i></div>');
            modal.modal('show');
        }
    });
}

jQuery(document).on('click', '#save-form-modal', function(e){
    $('#inputs-form').submit();
});

// jQuery(document).on('keypress', '#inputs-form', function(event){
//     if (event.which == 13) {
//         event.preventDefault();
//         $('#inputs-form').submit();
//     }
// });


jQuery(document).on('submit', '#inputs-form', function(e){
    var formData = $(this).serialize();
    
    // only works when allow drag/drop
    // var parent_node_id = $(drop_area).attr('id');
    var parent_node_id = 'fields_form_container'

    $.ajax({
        url: $(this).attr("action"),
        type: 'post',
        data: formData,

        beforeSend: function (xhr) {
          modal.find('.dimm').fadeIn(50);
        },
        success: function (response) {
            $.pjax.reload({container: '#inputs-list'});
            modal.modal('hide');
        },
        error: function(xhr){
            var err = eval("(" + xhr.responseText + ")");
            console.log(err.Message);
            $('#add-field-modal').find('.dimm').fadeOut(50);
        }
    });
    return false;
});

$('body').on('hidden.bs.modal', '.modal', function () {
    $(this).remove();
});
