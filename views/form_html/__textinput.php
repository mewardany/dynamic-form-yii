<?php

use wardany\dform\fields\FieldTextInput;
use rmrevin\yii\fontawesome\AssetBundle;
use wardany\dform\assets\IconAsset;
/* @var $this yii\web\View */
/* @var $model wardany\dform\models\Form */
/* @var $form yii\widgets\ActiveForm */
AssetBundle::register($this);
IconAsset::register($this);

?>
<?= $form->field($model, 'placeHolder')->textInput()->textInput() ?>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'showRightIcon')->textInput()->checkbox() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'rightIcon')->widget('\insolita\iconpicker\Iconpicker',[
          'iconset'=>'fontawesome',
          'clientOptions'=>['rows'=>8,'cols'=>10,'placement'=>'right'],
        ])->label(FieldTextInput::htmlElements('rightIcon')); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'rightIconText')->textInput()->label(FieldTextInput::htmlElements('rightIconText')) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'rightIconClass')->textInput()->label(FieldTextInput::htmlElements('rightIconClass')) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'showLeftIcon')->textInput()->checkbox() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'leftIcon')->widget('\insolita\iconpicker\Iconpicker',[
          'iconset'=>'fontawesome',
          'clientOptions'=>['rows'=>8,'cols'=>10,'placement'=>'right'],
        ])->label(FieldTextInput::htmlElements('leftIcon')); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'leftIconText')->textInput()->label(FieldTextInput::htmlElements('leftIconText')) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'leftIconClass')->textInput()->label(FieldTextInput::htmlElements('leftIconClass')) ?>
    </div>
</div>
