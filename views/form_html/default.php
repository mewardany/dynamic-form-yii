<?php
/* @var $this yii\web\View */
/* @var $model wardany\dform\models\Form */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'formOptionsClass')->textInput()->label('Container class') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'formInputOptionsClass')->textInput()->label('Input class') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'formErrorOptionsClass')->textInput()->label('Error class') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'hideLabel')->checkbox()->label('Hide label ?') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'formLabelOptionsClass')->textInput()->label('Label class') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'formHintOptionsClass')->textInput()->label('Hint class') ?>
    </div>
</div>
<?php if(file_exists(__DIR__.'/'.$extras_form.'.php')): ?>
    <?= $this->render($extras_form, ['form'=> $form, 'model'=>$model]) ?>
<?php endif; ?>
