<?php

use wardany\dform\fields\FieldTextInput;
use rmrevin\yii\fontawesome\AssetBundle;

/* @var $this yii\web\View */
/* @var $model wardany\dform\models\Form */
/* @var $form yii\widgets\ActiveForm */
AssetBundle::register($this);
?>
<?= $this->render('__textinput',['form'=> $form, 'model'=> $model]) ?>
