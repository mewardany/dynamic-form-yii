<?php

use wardany\dform\fields\FieldTextInput;
use rmrevin\yii\fontawesome\AssetBundle;
use wardany\dform\assets\IconAsset;
/* @var $this yii\web\View */
/* @var $model wardany\dform\models\Form */
/* @var $form yii\widgets\ActiveForm */
AssetBundle::register($this);
IconAsset::register($this);

?>
<?= $this->render('__textinput',['form'=> $form, 'model'=> $model]) ?>
