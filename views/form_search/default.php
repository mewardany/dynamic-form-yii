<?php
/* @var $this yii\web\View */
/* @var $model wardany\dform\models\Form */
/* @var $form yii\widgets\ActiveForm */
?>
<?= $form->field($model, 'allowSearch')->checkbox()->label(false) ?>
<?php if(file_exists(__DIR__.'/'.$extras_form.'.php')): ?>
    <?= $this->render($extras_form, ['form'=> $form, 'model'=>$model]) ?>
<?php endif; ?>
