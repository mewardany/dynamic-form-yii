<?php
use wardany\dform\helpers\FieldHelper;
/* @var $this yii\web\View */
/* @var $model wardany\dform\models\Form */
/* @var $form yii\widgets\ActiveForm */
?>

<?= $form->field($model, 'searchWidget')->dropDownList([
    FieldHelper::DROPDOWN_LIST=> 'dropDown List',
    FieldHelper::CHECKBOX_LIST=> 'CHECKBOX LIST',
], ['prompt'=> 'Leave as is']) ?>
