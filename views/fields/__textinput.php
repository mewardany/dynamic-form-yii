<?php

use yii\helpers\Html;
use insolita\iconpicker\Iconpicker;



/* @var $this yii\web\View */
/* @var $model wardany\dform\fields\FieldTextInput */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="field-text-input-form">
    
    <?= $form->field($model, 'required')->checkbox() ?>

    <?= $form->field($model, 'length')->textInput() ?>
    
    <?= $form->field($model, 'max')->textInput() ?>

    <?= $form->field($model, 'min')->textInput() ?>
    
</div>
