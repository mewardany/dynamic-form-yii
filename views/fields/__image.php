<?php

use dosamigos\fileinput\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model wardany\dform\fields\FieldImage */
/* @var $form yii\widgets\ActiveForm */
?>
<?= $form->field($model, 'required')->checkbox() ?>

    <div class='row'>
        <div class='col-sm-6'>
            <div class='row'>
                <div class='col-sm-12'>
                    <?= $form->field($model, 'extensions')->textInput() ?>
                </div>

                <div class='col-sm-12'>
                    <?= $form->field($model, 'maxFiles')->textInput() ?>
                </div>

                <div class='col-sm-6'>
                    <?= $form->field($model, 'minSize')->textInput() ?>
                </div>

                <div class='col-sm-6'>
                    <?= $form->field($model, 'maxSize')->textInput() ?>
                </div>

                <div class='col-sm-6'>
                    <?= $form->field($model, 'minWidth')->textInput() ?>
                </div>

                <div class='col-sm-6'>
                    <?= $form->field($model, 'maxWidth')->textInput() ?>
                </div>

                <div class='col-sm-6'>
                    <?= $form->field($model, 'minHeight')->textInput() ?>
                </div>

                <div class='col-sm-6'>
                    <?= $form->field($model, 'maxHeight')->textInput() ?>
                </div>
            </div>
        </div>
        <?php /*
        <div class='col-sm-6'>
            <?= FileInput ::widget([
                'model' => $model,
                'attribute' => 'placeHolder',
                'thumbnail' => $model->getThumbUploadUrl('placeHolder'),
                'style' => FileInput::STYLE_IMAGE,
                'options'=>['accept'=>'image/*']
            ]) ?>
        </div>
        */ ?>
    </div>
