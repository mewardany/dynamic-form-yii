<?php

use wardany\dform\assets\FormAsset;
use wardany\dform\fields\FieldTextInput;
use yii\widgets\Pjax;
use yii\jui\JuiAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \wardany\dform\models\Form */

rmrevin\yii\fontawesome\AssetBundle::register($this);

JuiAsset::register($this);

$this->title = 'Add fields to form: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Add fields';
?>


<div class="row">
    <div class="col-sm-12">
        <h1><?= $model->title ?></h1>
    </div>
    <div class="col-sm-2">
        <?= $this->render('_fields_buttons',['model'=> $model]) ?>
    </div>

    <div class="col-sm-10">
        <div class="dynamic_inputs_container">
            <?= \wardany\dform\widgets\Titles::widget(['model'=> $model]) ?>
        </div>
    </div>

</div>
