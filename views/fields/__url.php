<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model wardany\dform\fields\FieldUrl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="field-url-form">

    <?= $form->field($model, 'required')->checkbox() ?>

    <?= $form->field($model, 'validSchemes')->textInput(['maxlength' => true]) ?>

</div>