<?php

use wardany\dform\helpers\FieldHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
?>

<?php foreach (FieldHelper::fields() as $field => $options) {
     echo Html::tag(
            'p',
            '<span class="'.$options['icon'].'"></span> '.$options['title'],
            [
                'class'=> 'btn btn-default btn-block btn-social get-field',
                'id'=>'add-'.$field,
                'data-href'=> Url::to(['add-field', 'id'=> $model->id, 'field'=> $field]),
                'title'=>  $options['title'],
                // 'ondragstart'=> 'dragButton(event)',
                // 'ondrag'=> 'dragging(event)',
                // 'draggable'=> 'true'
            ]);
} ?>
