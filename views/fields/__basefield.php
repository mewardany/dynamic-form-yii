<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model wardany\dform\models\Field */
/* @var $form yii\widgets\ActiveForm */
?>
<?= $form->field($model, 'attribute_label')->textInput() ?>

<?php if($model->isNewRecord): ?>  
    <?= $form->field($model, 'attribute_name')->widget(\wardany\slugify\Slugify::className(),['source_attribute'=>'attribute_label', 'separator'=>'_', 'disable'=> false]) ?>
<?php endif; ?>