<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
?>
<div class="panel panel-default"  id="form-data" style="display: none">
    <div class="panel-heading">
        <h3 class="panel-title" id="form-title"></h3>
    </div>
    <div class="panel-body">
        <div id="form-errors">

        </div>
        <div id="form-body">

        </div>
    </div>
    <div class="panel-footer">
        <?= Html::button(Yii::t('app', 'Save'),['id'=>'save-form-modal', 'class'=>'btn btn-primary']) ?>
    </div>
</div>