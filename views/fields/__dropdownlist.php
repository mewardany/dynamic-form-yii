<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wardany\dform\fields\FieldDropdownList;

/* @var $this yii\web\View */
/* @var $model wardany\dform\fields\FieldDropdownList */
/* @var $form yii\widgets\ActiveForm */
?>

    <?= $form->field($model, 'required')->checkbox() ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'items')->textarea(['rows'=>6]) ?>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'endValueType')->dropDownList(FieldDropdownList::itemTypes(),['prompt'=> 'end value']) ?>
                    <?= $form->field($model, 'endValue')->textInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'startValueType')->dropDownList(FieldDropdownList::itemTypes(),['prompt'=> 'start value']) ?>
                    <?= $form->field($model, 'startValue')->textInput() ?>
                </div>
                
            </div>
        </div>
    </div>