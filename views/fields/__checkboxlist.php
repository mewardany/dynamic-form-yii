<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model wardany\dform\fields\FieldCheckboxList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="field-checkbox-list-form">

    <?= $form->field($model, 'required')->checkbox() ?>

    <?= $form->field($model, 'items')->textarea(['rows' => 6]) ?>

</div>
