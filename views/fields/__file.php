<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model wardany\dform\fields\FieldFile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="field-file-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'required')->textInput() ?>

    <?= $form->field($model, 'extensions')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'mime_types')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'min_size')->textInput() ?>

    <?= $form->field($model, 'max_size')->textInput() ?>

    <?= $form->field($model, 'max_files')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
