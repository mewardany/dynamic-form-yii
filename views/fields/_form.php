<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model wardany\dform\models\Form */
/* @var $form yii\widgets\ActiveForm */


?>
<?php
    Modal::begin([
        'options'=>['id'=>'add-field-modal'],
        'header'=>'<h3 class="panel-title" id="form-title"></h3>',
        'toggleButton' => false,
        'footer'=> Html::button(Yii::t('app', 'Save'),['id'=>'save-form-modal', 'class'=>'btn btn-primary'])
    ]);
?>
<div id="form-errors"></div>
<div class="form-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'inputs-form']]); ?>
    <div class="panel with-nav-tabs panel-default">
        <?= Tabs::widget([
            'itemOptions'=>['class'=> 'panel-body'],
            'items'=>[
                [
                    'label'=>   '<span class="glyphicon glyphicon-pencil"></span> Basics',
                    'content'=> $this->render('__basefield', ['model'=> $model, 'form'=> $form]).
                                $this->render($extras_form, ['model'=> $model, 'form'=> $form]),
                    'active'=>  true
                ],
                [
                    'label'=>   '<span class="glyphicon glyphicon-eye-open"></span> Styling',
                    'content'=> $this->render('@wardany/dform/views/form_html/default', ['model'=> $model, 'form'=> $form, 'extras_form'=>$extras_form])
                ],
                [
                    'label'=>   '<span class="glyphicon glyphicon-search"></span> Search options',
                    'content'=> $this->render('@wardany/dform/views/form_search/default', ['model'=> $model, 'form'=> $form, 'extras_form'=>$extras_form])
                ]
            ],
            'encodeLabels'=> false
        ]);
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
    Modal::end();
?>
