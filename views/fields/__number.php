<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model wardany\dform\fields\FieldNumber */
/* @var $form yii\widgets\ActiveForm */
?>

<?= $form->field($model, 'required')->checkbox() ?>

<?= $form->field($model, 'type')->dropDownList($model->types(),['prompt'=> 'Select type']) ?>

<?= $form->field($model, 'max')->textInput() ?>

<?= $form->field($model, 'min')->textInput() ?>
