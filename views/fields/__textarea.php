<?php

use wardany\dform\fields\FieldTextarea;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model FieldTextarea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="field-textarea-form">

    <?= $form->field($model, 'required')->checkbox() ?>

    <?= $form->field($model, 'placeHolder') ?>
    
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'editorType')->dropDownList(FieldTextarea::textareaEditorTypes()) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'max')->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'min')->textInput() ?>
        </div>
    </div>

</div>
