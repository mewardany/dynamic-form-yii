<?php

use yii\db\Migration;

class m170215_125804_add_field_form_foreign_key extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_form_fields', 'form_field', 'form_id', 'form', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_form_fields', 'form_field');
    }
}
