<?php

use yii\db\Migration;

class m170208_151928_create_form_table extends Migration
{
    public function up()
    {
        $this->createTable('form', [
            'id' => $this->primaryKey(),
            'form_options'=>  $this->text(),
            'view_options'=>  $this->text(),
            'validation'=> $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('form');
    }
}
