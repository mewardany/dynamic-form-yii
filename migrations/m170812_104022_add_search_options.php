<?php

use yii\db\Migration;

class m170812_104022_add_search_options extends Migration
{
    public function safeUp()
    {
        $this->addColumn('form_field', 'search_options', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('form_field', 'search_options');
    }

}
