<?php

use yii\db\Migration;
/**
 * Handles the creation of table `form`.
 */
class m170208_153702_inputs_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // Main input 
        $this->createTable('form_field', [ 
            'id'=>  $this->primaryKey(),
            'form_id'=>  $this->integer(),
            'field_id'=> $this->integer(),
            'field_type'=> $this->string()->notNull(),
            'attribute_name'=> $this->string(),
            'attribute_label'=> $this->string(),
            'view_title'=> $this->string(),
            'field_options'=> $this->text(),
            'view_options'=> $this->text(),
            'field_as_json'=> $this->text(),
            'field_as_html'=> $this->text(),
            'view_as_json'=> $this->text(),
            'view_as_html'=> $this->text(),            
        ]);
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('form_field');
    }
}
