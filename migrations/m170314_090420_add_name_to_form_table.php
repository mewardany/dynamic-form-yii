<?php

use yii\db\Migration;

class m170314_090420_add_name_to_form_table extends Migration
{
    public function up()
    {
        $this->addColumn('form', 'html_name', $this->string());
    }

    public function down()
    {
        $this->dropColumn('form', 'html_name');
    }

}
