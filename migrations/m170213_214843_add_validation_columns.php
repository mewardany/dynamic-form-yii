<?php

use yii\db\Migration;

class m170213_214843_add_validation_columns extends Migration
{
    public function up()
    {
        $this->addColumn('form_field', 'server_validations', $this->text());
        $this->addColumn('form_field', 'client_validations', $this->text());
    }

    public function down()
    {
        $this->dropColumn('form_field', 'server_validations');
        $this->dropColumn('form_field', 'client_validations');
    }
}
