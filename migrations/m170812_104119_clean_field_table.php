<?php

use yii\db\Migration;

class m170812_104119_clean_field_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('form_field', 'field_id');
        $this->dropColumn('form_field', 'view_title');
        $this->dropColumn('form_field', 'field_as_json');
        $this->dropColumn('form_field', 'field_as_html');
        $this->dropColumn('form_field', 'view_as_json');
        $this->dropColumn('form_field', 'view_as_html');
        $this->dropColumn('form_field', 'server_validations');
        $this->dropColumn('form_field', 'client_validations');
    }

    public function safeDown()
    {
        $this->addColumn('form_field', 'field_id', $this->integer());
        $this->addColumn('form_field', 'view_title', $this->string());
        $this->addColumn('form_field', 'field_as_json', $this->text());
        $this->addColumn('form_field', 'field_as_html', $this->text());
        $this->addColumn('form_field', 'view_as_json', $this->text());
        $this->addColumn('form_field', 'view_as_html', $this->text());
        $this->addColumn('form_field', 'server_validations', $this->text());
        $this->addColumn('form_field', 'client_validations', $this->text());
    }
}
