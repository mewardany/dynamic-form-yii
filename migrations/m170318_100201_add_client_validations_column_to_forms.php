<?php

use yii\db\Migration;

class m170318_100201_add_client_validations_column_to_forms extends Migration
{
    public function up()
    {
        $this->renameColumn('form', 'validation', 'server_validations');
        $this->addColumn('form', 'client_validations', $this->text());
    }

    public function down()
    {
        $this->renameColumn('form', 'server_validations', 'validation');
        $this->dropColumn('form', 'client_validations');
    }
}
