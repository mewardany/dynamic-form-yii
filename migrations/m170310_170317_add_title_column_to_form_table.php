<?php

use yii\db\Migration;

/**
 * Handles adding title to table `form`.
 */
class m170310_170317_add_title_column_to_form_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up(){
        $this->addColumn('form', 'title', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down(){
        $this->dropColumn('form', 'title');
    }
}
