<?php

use yii\db\Migration;

class m170212_122947_add_foreign_keys extends Migration
{
    public function up()
    {
        $this->createIndex('field_indexing', 'form_field', 'field_id');
    }

    public function down()
    {
        $this->dropIndex('field_indexing', 'form_field');

    }
}
