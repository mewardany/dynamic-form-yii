<?php

use yii\db\Migration;

class m170308_160501_add_form_option_field extends Migration
{
    public function up()
    {
        $this->addColumn('form_field' ,'form_options', $this->text());
    }

    public function down()
    {
        $this->dropColumn('form_field' ,'form_options');
    }
}
