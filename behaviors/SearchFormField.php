<?php
namespace wardany\dform\behaviors;
use yii;
use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;
use yii\helpers\Json;
use yii\validators\Validator;

/**
 * Description of DynamicFormField
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */
class SearchFormField extends AttributeBehavior{
    
      public function events() {
          return[
              BaseActiveRecord::EVENT_AFTER_FIND    => 'afterFind',
          ];
      }

      public function afterFind()
      {
          $search_options = Json::decode($this->owner->search_options);
          if(!empty($search_options)){
              $this->owner->_search_options = $search_options;
          }
      }

    public function getSearchWidget(){
        return $this->owner->_search_options['search_widget'];
    }

    public function setSearchWidget($value){
        return $this->owner->_search_options['search_widget'] = $value;
    }

    public function getAllowSearch(){
        return $this->owner->_search_options['allow_search'];
    }

    public function setAllowSearch($value){
        return $this->owner->_search_options['allow_search'] = $value;
    }
}
