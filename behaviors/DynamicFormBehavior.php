<?php
namespace wardany\dform\behaviors;

use Yii;
use wardany\dform\helpers\ViewHelper;
use wardany\dform\helpers\ImageHelper;
use wardany\dform\helpers\FieldHelper;
use wardany\dform\helpers\UploadHelper;
use wardany\dform\models\Field;
use wardany\dform\models\Form;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\base\UnknownPropertyException;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\validators\Validator;
use yii\db\BaseActiveRecord;
use yii\web\UploadedFile;
use yii\db\Query;
/**
 * Description of DynamicFormBehavior
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */
class DynamicFormBehavior extends Behavior{
    /**
     * @var ActiveRecord the owner of this behavior
     */
    public $owner;

    /**
     * @var \yii\db\ActiveQuery
     */
    public $form_relation = 'form' ;

    /**
     * @var Form
     */
    private $form ;

    /**
     * @var string
     */
    public $related_values_name ;

    /**
     * @var string
     */
    public $link ;

    /**
     * @var \yii\db\ActiveQuery
     */
    public $values_relation ;

    /**
     * @var \ArrayObject [attribute_name => valu]
     */
    public $extra_attributes ;

    private $files ;

    public function events() {
        return[
            BaseActiveRecord::EVENT_INIT            => 'start',
            BaseActiveRecord::EVENT_AFTER_FIND      => 'afterFind',
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            BaseActiveRecord::EVENT_AFTER_INSERT    => 'afterSave',
            BaseActiveRecord::EVENT_AFTER_UPDATE    => 'afterSave',
            BaseActiveRecord::EVENT_AFTER_DELETE    => 'afterDelete',
        ];
    }

    public function start()
    {
        if($this->owner && $this->owner->category_id && $this->owner->isNewRecord){
            $this->_attach($this->owner);
        }
    }

    public function afterFind()
    {
        $this->_attach($this->owner);
    }

    /**
     * Attaches the behavior object to the component.
     * The default implementation will set the [[owner]] property
     * and attach event handlers as declared in [[events]].
     * Make sure you call the parent implementation if you override this method.
     * @param ActiveRecord $owner the component that this behavior is to be attached to.
     */
    public function _attach($owner)
    {
        parent::attach($owner);
        $this->form = $this->owner->{$this->form_relation};
        $this->values_relation = $owner->getRelation($this->related_values_name) ;
        $this->link= key($this->values_relation->link);
        $this->extra_attributes = $this->getExtraAttributes();
        if($owner->scenario != 'search')
            $this->createValidators();

    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge($this->owner->attributeLabels(),
            Field::find()
            ->select('attribute_label')
            ->where(['form_id'=> $this->form->id])
            ->indexBy('attribute_name')
            ->column()
        );
    }

    /**
     * Creates validator objects based on the validation rules specified in [[rules()]].
     * Unlike [[getValidators()]], each time this method is called, a new list of validators will be returned.
     * @throws InvalidConfigException if any validation rule configuration is invalid
     */
    public function createValidators()
    {
        $rules = !empty($this->form->server_validations)? Json::decode($this->form->server_validations): [];
        $validators = $this->owner->getValidators();
        foreach ($rules as $rule) {
            if ($rule instanceof Validator) {
                $this->owner->validators[] = $rule;
            } elseif (is_array($rule) && isset($rule[0], $rule[1])) { // attributes, validator type
                $validator = Validator::createValidator($rule[1], $this, (array) $rule[0], array_slice($rule, 2));
                $this->owner->validators[] = $validator;
            } else {
                throw new InvalidConfigException('Invalid validation rule: a rule must specify both attribute names and validator type.');
            }
        }
    }

    public function canGetProperty($name, $checkVars = true) {
        return parent::canGetProperty($name) || key_exists($name, $this->extra_attributes);
    }

    public function canSetProperty($name, $checkVars = true) {
        return parent::canSetProperty($name) || key_exists($name, $this->extra_attributes);
    }

    public function __get($name) {
        try {
            return parent::__get($name);
        } catch (UnknownPropertyException $exc) {
            if(key_exists($name, $this->extra_attributes)){
                $value = $this->extra_attributes[$name];
                if($this->isJson($value))
                    return Json::decode($value);
                return $value;
            }
            throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value) {
        try {
            parent::__set($name, $value);
        } catch (UnknownPropertyException $exc) {
            if(key_exists($name, $this->extra_attributes)){
                $value = is_array($value)? Json::encode($value): $value;
                $this->extra_attributes[$name] = $value;
            }
            else
                throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    function isJson(...$args) {
        json_decode(...$args);
        return (json_last_error()===JSON_ERROR_NONE);
    }

    public function getView($attribute){
        return new ViewHelper($this->owner, $attribute);
    }

    /**
     * @return UploadHelper ;
     */
    public function file(){
        return new UploadHelper($this->owner);
    }

    /**
     * @return ImageHelper
     */
    public function image(){
        return new ImageHelper($this->owner);
    }

    /**
     *
     * @return \ArrayObject [attribute_name=>value]
     */
    public function getExtraAttributes(){
        if(! $this->owner->form)
            return [];
        $values = [];
        $direct_values = $this->getDirectValues()->all();
        $translate_values = $this->getTranslateValues()->all();
        $list_values = $this->getListValues()->all();

        foreach ($direct_values as $content) {
            $values[$content->attribute_name]= $content->value;
        }

        foreach ($translate_values as $content) {
            $values[$content->attribute_name]= Yii::t($content->translation_category, $content->value);
        }

        foreach ($list_values as $content) {
            $items = $this->isJson($content->value)? $this->formalizingArray(json_decode($content->value), $content->translation_category): '';
            $values[$content->attribute_name]= \yii\helpers\Html::tag('ul', $items);
        }
        return $values;
    }

    private function getAllValues(){
        $select = ['f.id', 'f.attribute_name', 'f.attribute_label', 'value'=> new Expression('NULL')];
        $query = Field::find()
                ->alias('f')
                ->where(['f.form_id'=> $this->owner->form->id]);
        if($this->owner->id){
            $select['value']= 'v.value';
            $query->join('LEFT OUTER JOIN','post_details v', 'v.field_id = f.id')
                ->andWhere(['v.post_id'=> $this->owner->id]);
        }
        $query->select($select);
        return $query;
    }

    // direct : textarea, textinput, number, email, url, phone
    private function getDirectValues(){
        return $this->getAllValues()->andWhere(
            ['f.field_type'=> [
                FieldHelper::TEXT_INPUT,
                FieldHelper::TEXT_AREA,
                FieldHelper::NUMBER,
                FieldHelper::EMAIL,
                FieldHelper::URL,
                FieldHelper::PHONE,
            ]]);
    }

    //dropdownList, radioList
    private function getTranslateValues(){
        return $this->getAllValues()->andWhere(
            ['f.field_type'=> [
                FieldHelper::DROPDOWN_LIST,
                FieldHelper::RADIO_LIST,
            ]]);
    }

    //checkbox
    private function getListValues(){
        return $this->getAllValues()->andWhere(
            ['f.field_type'=> FieldHelper::CHECKBOX_LIST]);
    }

    protected function formalizingArray($items, $translation_category) {
        if (!is_array($items))
          return null;
        $s_items ='';
        foreach ($items as $item) {
            $s_items.= '<li>'. Yii::t($translation_category, $item).'</li>';
        }
        return $s_items;
    }

    public function getRelatedFields(){
        return $this->form?Field::find()->where(['form_field.form_id'=> $this->form->id]): [] ;
    }

    public function beforeValidate(){
//        $images = FieldImage::find()->select(['id', 'attribute_name', 'field_options'])->where(['field_type'=> FieldHelper::IMAGE])->all();
//        // $fields = Field::find()->select(['id', 'attribute_name'])->where(['field_type'=> FieldHelper::IMAGE])->orWhere(['field_type'=> FieldHelper::FILE])->all();
//        foreach ($images as $image) {
//            if($image->maxFiles > 1)
//                $this->extra_attributes[$image->attribute_name] = UploadedFile::getInstances($this->owner, $image->attribute_name);
//            else
//                $this->extra_attributes[$image->attribute_name] = UploadedFile::getInstance($this->owner, $image->attribute_name);
//        }
    }

    public function afterSave() {
        $this->owner->unlinkAll($this->related_values_name, true);
        foreach ($this->extra_attributes as $attribute => $value) {
            $field = Field::find()->where(['form_id'=> $this->form->id, 'attribute_name'=> $attribute])->one();
            if($field){
                $model = new $this->values_relation->modelClass ;
                $model->field_id = $field->id ;
                $model->value = $this->getPostedValue($value) ;
                $this->owner->link($this->related_values_name, $model);

            }
        }
    }

    public function afterDelete() {
        $this->owner->unlinkAll($this->related_values_name);
    }

    private function getPostedValue($value){
        $uploader = new UploadHelper($this->owner);
        if($value instanceof UploadedFile)
            return(UploadHelper::handleFiles($value));
        if(is_array($value) && $value[0] instanceof UploadedFile){
            return UploadHelper::handleFiles($value);
        }
        return $value;
    }
}
