<?php
namespace wardany\dform\behaviors;

use yii\base\Component;
use yii\helpers\Html;

/**
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */
class DynamicFormView extends Component {

    public $owner ;

    public $attribute;

    private $values_model ;

    public function __construct($owner, $attribute, $config = []){
        $this->owner = $owner;
        $this->attribute = $attribute;
        $this->values_model = $owner->getRelation($this->owner->related_values_name)->innerJoin('form_field ff')->where(['ff.attribute_name'=> $attribute])->one();
        parent::__construct($config);
    }

    public function value(){
        return $this->owner->{$this->attribute};
    }

    public function asImage($profile = null, $options = [], $containerOptions = false){
        $url = $this->image->uploadPath() ;
        $files = json_decode($this->owner->{$this->attribute});
        $images = '';
        if (json_last_error() === JSON_ERROR_NONE) {
            foreach ($files as $image) {
                $images .= Html::img($url.'/'.$image, $options);
            }
        }
        else{
            $images = Html::img($url.'/'.$this->owner->{$this->attribute}, $option);
        }

        return $containerOptions? Html::tag('div', $images, $containerOptions): $images;
    }



}
