<?php
namespace wardany\dform\behaviors;
use yii;
use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\validators\Validator;

/**
 * Description of DynamicFormField
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */
class DynamicFormField extends AttributeBehavior{

  public $translation_category = 'dform_labels';

    // public $_form_options;

    public function events() {
        return[
            BaseActiveRecord::EVENT_INIT    => 'init',
            // BaseActiveRecord::EVENT_AFTER_FIND    => 'afterFind',
            // BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate'
        ];
    }

    public function attach($owner){
        parent::attach($owner);
        $validator = Validator::createValidator('string',$this->owner,['formOptionsClass', 'formInputOptionsClass', 'formErrorOptionsClass', 'formLabelOptionsClass', 'formHintOptionsClass'],['max'=> 255]);
        $this->owner->validators[] = $validator;
    }

    // public function afterFind(){
    //     // $form_options = $this->owner->form_options? Json::decode($this->owner->form_options): [];
    //     // $this->owner->_form_options = ArrayHelper::merge($this->owner->_form_options, $form_options);
    //
    // }
    //
    // public function beforeValidate(){
    //     $this->owner->form_options = Json::encode($this->owner->_form_options);
    //     return true;
    // }

    public function getFormOptions(){
        return[
            'class'=> $this->getFormOptionsClass()
        ];
    }

    public function getFormInputOptions(){
        $options = [
            'class'=> $this->getFormInputOptionsClass()
        ];
        try {
            $options['placeHolder'] = $this->getPlaceHolder();
            $options['prompt'] = $this->getPrompt();
            return $options;
        } catch (yii\base\ErrorException $e) {

        }

        return $options;
    }

    public function getFormLabelOptions(){
        return[
            'class'=> $this->getFormLabelOptionsClass()
        ];
    }

    public function getFormHintOptions(){
        return[
            'class'=> $this->getFormHintOptionsClass()
        ];
    }

    public function getFormErrorOptions(){
        return[
            'class'=> $this->getFormErrorOptionsClass()
        ];
    }

    /**
     * @return int order of field inside parent node in form
     */
    public function getFormOrder() {
       return $this->owner->_form_options['order'];
    }

    /**
     * @param int $value order of field inside parent node in form
     */
    public function setFormOrder($value) {
        $this->owner->_form_options['order'] = $value;
    }

    /**
     * return parent node id
     * @return string parent node id in form to store fields inside
     */
    public function getFormParentNodeId() {
        return $this->owner->_form_options['parent_node_id'];
    }

    /**
     * @param string $value parent node id
     */
    public function setFormParentNodeId($value) {
        $this->owner->_form_options['parent_node_id'] = $value;
    }

    /**
     * return either to hide label or not
     * @return bool false to show label true to hide label
     */
    public function getHideLabel() {
        return $this->owner->_form_options['hide_label'];
    }

    /**
     * @param bool hide form label
     */
    public function setHideLabel($value) {
        $this->owner->_form_options['hide_label'] = $value;
    }

    /**
     * return html placeHolder
     * @return string place holder
     */
    public function getPlaceHolder() {
      return Yii::t($this->translation_category, $this->owner->_form_options['place_holder']);
    }

    /**
     * @param string placeHolder
     */
    public function setPlaceHolder($value) {
        $this->owner->_form_options['place_holder'] = $value;
    }

    /**
     * return html prompt
     * @return string prompt
     */
    public function getPrompt() {
      return Yii::t($this->translation_category, $this->owner->_form_options['prompt']);
    }

    /**
     * @param string prompt
     */
    public function setPrompt($value) {
        $this->owner->_form_options['prompt'] = $value;
    }

   

    /**
    * container class attribute default form-group
    * @return string css class or null if no class
    */
    public function getFormOptionsClass(){
        return $this->owner->_form_options['options']['class'];
    }

    /**
    * @param string $value container css class attribute
    */
    public function setFormOptionsClass($value) {
        $this->owner->_form_options['options']['class'] = $value;
    }

    /**
     * input css class default : form-controle
     * @return string css class
     */
     public function getFormInputOptionsClass(){
        return $this->owner->_form_options['inputOptions']['class'];
    }

    /**
     * @param string $value input css class
     */
    public function setFormInputOptionsClass($value) {
        $this->owner->_form_options['inputOptions']['class'] = $value;
    }

    /**
     * error css class default : help-block
     * @return string error css class
     */
     public function getFormErrorOptionsClass(){
        return $this->owner->_form_options['errorOptions']['class'];
    }

    /**
     * @param string $value error css class
     */
    public function setFormErrorOptionsClass($value) {
        $this->owner->_form_options['errorOptions']['class'] = $value;
    }

    /**
     * label css class default : controle-label
     * @return string label css class
     */
     public function getFormLabelOptionsClass(){
        return $this->owner->_form_options['labelOptions']['class'];
    }

    /**
     * @param string $value label css class
     */
    public function setFormLabelOptionsClass($value) {
        $this->owner->_form_options['labelOptions']['class'] = $value;
    }

    /**
    * hint css class default : hint-block
     * @return string hint css class
     */
     public function getFormHintOptionsClass(){
        return $this->owner->_form_options['hintOptions']['class'];
    }

    /**
     * @param string $value hint css class
     */
    public function setFormHintOptionsClass($value) {
        $this->owner->_form_options['hintOptions']['class'] = $value;
    }

    public function getShowRightIcon(){
        return $this->owner->_form_options['right_icon']['show'];
    }

    public function setShowRightIcon($value){
        return $this->owner->_form_options['right_icon']['show'] = $value;
    }

    public function getRightIcon(){
        return $this->owner->_form_options['right_icon']['icon'];
    }

    public function setRightIcon($value){
        return $this->owner->_form_options['right_icon']['icon'] = $value;
    }

    public function getRightIconClass(){
        return $this->owner->_form_options['right_icon']['class'];
    }

    public function setRightIconClass($value){
        return $this->owner->_form_options['right_icon']['class'] = $value;
    }

    public function getRightIconText(){
        return $this->owner->_form_options['right_icon']['text'];
    }

    public function setRightIconText($value){
        return $this->owner->_form_options['right_icon']['text'] = $value;
    }

    public function getShowLeftIcon(){
        return $this->owner->_form_options['left_icon']['show'];
    }

    public function setShowLeftIcon($value){
        return $this->owner->_form_options['left_icon']['show'] = $value;
    }

    public function getLeftIcon(){
        return $this->owner->_form_options['left_icon']['icon'];
    }

    public function setLeftIcon($value){
        return $this->owner->_form_options['left_icon']['icon'] = $value;
    }

    public function getLeftIconClass(){
        return $this->owner->_form_options['left_icon']['class'];
    }

    public function setLeftIconClass($value){
        return $this->owner->_form_options['left_icon']['class'] = $value;
    }

    public function getLeftIconText(){
        return $this->owner->_form_options['left_icon']['text'];
    }

    public function setLeftIconText($value){
        return $this->owner->_form_options['left_icon']['text'] = $value;
    }
}
