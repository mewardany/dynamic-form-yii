<?php
namespace wardany\dform\behaviors;

/**
 * Description of SluggableBehavior
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */

use yii\base\InvalidConfigException;
use yii\behaviors\SluggableBehavior as YiiSluggableBehavior;
use yii\helpers\Inflector;

class SluggableBehavior extends YiiSluggableBehavior{
    
    /**
     * This method is called by [[getValue]] to generate the slug.
     * You may override it to customize slug generation.
     * The default implementation calls [[\yii\helpers\Inflector::slug()]] on the input strings
     * concatenated by dashes (`-`).
     * @param array $slugParts an array of strings that should be concatenated and converted to generate the slug value.
     * @return string the conversion result.
     */
    protected function generateSlug($slugParts)
    {
        return Inflector::slug(implode('_', $slugParts),'_');
    }
    
    /**
     * Generates slug using configured callback or increment of iteration.
     * @param string $baseSlug base slug value
     * @param integer $iteration iteration number
     * @return string new slug value
     * @throws InvalidConfigException
     */
    protected function generateUniqueSlug($baseSlug, $iteration)
    {
        if (is_callable($this->uniqueSlugGenerator)) {
            return call_user_func($this->uniqueSlugGenerator, $baseSlug, $iteration, $this->owner);
        }
        return $baseSlug . '_' . ($iteration + 1);
    }
}
