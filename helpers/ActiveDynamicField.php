<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace wardany\dform\helpers;

use dosamigos\ckeditor\CKEditor;
use wardany\dform\fields\FieldTextarea;
use wardany\dform\models\Field;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
/**
 * Description of ActiveDynamicField
 *
 * @author Muhammad Wardany <muhammad.wardany@gmail.com>
 */
class ActiveDynamicField extends \yii\widgets\ActiveField{
    /**
     * @var Field the field model that this field is associated with.
     */
    public $field;

    public $allow_hide_label = true;

    public $hide_prompt = false;

    /**
     * Generates a label tag for [[attribute]].
     * @param null|string|false $label the label to use. If `null`, the label will be generated via [[Model::getAttributeLabel()]].
     * If `false`, the generated field will not contain the label part.
     * Note that this will NOT be [[Html::encode()|encoded]].
     * @param null|array $options the tag options in terms of name-value pairs. It will be merged with [[labelOptions]].
     * The options will be rendered as the attributes of the resulting tag. The values will be HTML-encoded
     * using [[Html::encode()]]. If a value is `null`, the corresponding attribute will not be rendered.
     * @return $this the field object itself.
     */
    public function label($label = null, $options = [])
    {
        if($this->field){
            if($this->allow_hide_label && $this->field->getHideLabel()){
                $this->parts['{label}'] = '';
                return $this;
            }
            $options = array_merge($this->labelOptions, $options);
            if ($label !== null) {
                $options['label'] = $label;
            }
            $this->parts['{label}'] = $this->getDynamicLabel($this->model, $this->attribute, $options);
            return $this;
        }
        else
            return parent::label ($label, $options);
    }

    public function getDynamicLabel($model, $attribute, $options = []){
        $for = ArrayHelper::remove($options, 'for', Html::getInputId($model, $attribute));
        $attribute = Html::getAttributeName($attribute);
        $label = ArrayHelper::remove($options, 'label', $this->field->tLabel);
        return Html::label($label, $for, $options);
    }

    public function dynamicTextInput($options = [])
    {
        $this->setPlaceHolder($options);
        $this->addIconToTemplate();
        return parent::textInput($options);
    }

    public function dynamicTextarea($options = [])
    {
        $this->setPlaceHolder($options);
        switch ($this->field->editorType) {
            case FieldTextarea::TEXTAREA_BASIC :
                return $this->ckeditor('basic');
            case FieldTextarea::TEXTAREA_STANDARD :
                return $this->ckeditor('standard');
            case FieldTextarea::TEXTAREA_FULL :
                return $this->ckeditor('full');
            default:
                return parent::textarea($options);
        }
    }

    public function dynamicCheckboxList($options = [])
    {
        $item = function($index, $label, $name, $checked, $value) {
                    $return  = '<label class="custom-control custom-checkbox">';
                    $return .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" class="custom-control-input">';
                    $return .= '<span class="custom-control-indicator"></span>';
                    $return .= '<span class="custom-control-description">' . $label . '</span>';
                    $return .= '</label>';

                    return $return;
                };
        $options['item']= $item;
        return parent::checkboxList($this->field->getItemsAsArray(), $options);
    }

    public function dynamicDropDownList($options = [])
    {
        if(!$this->hide_prompt)
            $options['prompt'] = $this->field->getPrompt();
        return parent::dropDownList($this->field->getItemsAsArray(), $options);
    }

    public function dynamicRadioList($options = [])
    {
        $item = function($index, $label, $name, $checked, $value) {
                    $return  = '<label class="custom-control custom-radio">';
                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" class="custom-control-input">';
                    $return .= '<span class="custom-control-indicator"></span>';
                    $return .= '<span class="custom-control-description">' . $label . '</span>';
                    $return .= '</label>';

                    return $return;
                };
        $options['item']= $item;
        return parent::radioList($this->field->getItemsAsArray(), $options);
    }

    public function setPlaceHolder(&$options = []) {
        $options['placeHolder'] = $this->field->getPlaceHolder();
    }

    public function setPrompt(&$options = []) {
        $options['prompt'] = $this->field->getPrompt();
    }

    public function addIconToTemplate() {
        $icon = false ; $right_icon = null; $left_icon = null;
        if($this->field->showRightIcon){
                $right_icon = Html::tag('span', '<i class="glyphicon '.Html::encode($this->field->rightIcon).'"></i> '. Html::encode($this->field->rightIconText), ['class'=> 'input-group-addon '.Html::encode($this->field->rightIconClass)]);
                $icon = true;
        }
        if($this->field->showLeftIcon){
                $left_icon = Html::tag('span', Html::encode($this->field->leftIconText). ' <i class="glyphicon '. Html::encode($this->field->leftIcon). '"></i>', ['class'=> 'input-group-addon '. Html::encode($this->field->leftIconClass)]);
                $icon = true;
        }
        if($icon)
            $this->template = str_replace ('{input}', '<div class="input-group">'.$left_icon.'{input}'.$right_icon.'</div>', $this->template);
    }

    public function ckeditor($preset) {
        return $this->form->field($this->model, $this->field->attribute_name)->widget(CKEditor::className(), [
                'preset' => $preset
            ]);
    }
}
