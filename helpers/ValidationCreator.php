<?php

namespace wardany\dform\helpers;
use Yii;
use yii\base\Component;
use yii\validators\StringValidator;


/**
 * Description of FieldHelper
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */

class ValidationCreator extends Component{
    private $rules = [];
    private $attribute_name;

    /**
     * create an object of validation creator and assign attribute name value
     * @param string $attribute_name
     * @return $this the model instance itself.
     */
    public static function addRules($attribute_name){
        $validation_creator = new self;
        $validation_creator->setAttributeName($attribute_name);
        return $validation_creator;
    }

    /**
     * return array of rules
     * @return array $this->rules
     */
    public function getRules(){
        return $this->rules;
    }

    /**
     * set attribute name value
     * @param string  $attribute_name
     */
    public function setAttributeName($attribute_name){
        $this->attribute_name = $attribute_name;
    }

    /**
     * add 'require' validator to rules
     * @param  bool $isRequired
     * @return $this the model instance itself
     */
    public function requiredValidator($isRequired= null){
        if($isRequired)
            $this->rules[] = [$this->attribute_name, 'required'];
        return $this;
    }

    /**
     * add 'string' validator to rules
     * @param  int $max
     * @param  int $min
     * @param  int $length
     * @return $this the model instance itself
     */
    public function stringValidator($max= null, $min= null, $length= null){
        $string_validator = [$this->attribute_name, 'string', 'max'=> $max];
        if(is_int($min))
            $string_validator['min'] = $min;
        if(is_int($length))
            $string_validator['length'] = $length;
        $this->rules[] = $string_validator;
        return $this;
    }

    /**
     * @param  string $validSchemes http , https or both
     * @return $this the model instance itself
     */
    public function urlValidator($validSchemes= null){
        $validator = [$this->attribute_name, 'url'];
        if($validSchemes)
            $validator['validSchemes'] = explode(',', $validSchemes);

        $this->rules[] = $validator;
        return $this;
    }
}
