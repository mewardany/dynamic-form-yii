<?php

namespace wardany\dform\helpers;
use Yii;
use yii\base\Component;
use yii\helpers\Json;
use yii\helpers\Inflector;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\UploadedFile;

/**
 * Description of UploadHelper
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */

class UploadHelper extends Component{

    private $model;

    public function __construct($model, $config = []) {
        $this->model = $model;
    }

    public function handleFiles($files){
        if($files instanceof UploadedFile){
            return $this->uploadFile($file);
        }
        $file_name = [];
        foreach ($files as $file) {
            $file_name[] = $this->uploadFile($file);
        }
        return Json::encode($file_name);
    }

    public function uploadFile($file){

        $file_name = $this->generateFileName($file);
        $file->saveAs($this->getUploadPath().'/'.$file_name);
        return $file_name;
    }

    public function getFile($attribute_name){
        $attribute = $this->model->{$attribute_name};

        return $this->getUploadUrl().'/'.$attribute;
    }

    

    public function getUploadPath() {
        $path = $this->resolvePath();
        if(!is_dir($path))
            FileHelper::createDirectory($path, $mode = 0777, $recursive = true);
        return $path ;
    }

    public function getUploadUrl() {
        return $this->resolveUrl() ;
    }

    public function resolvePath(){
        $dir = Yii::getAlias(Yii::$app->getModule('w_forms')->upload_path);
        $path = $dir.'/'. $this->uploadDir() .'/'.$this->model->id;
        return $path;
    }

    public function resolveUrl(){
        $dir = Yii::$app->getModule('w_forms')->upload_url;
        return  $dir.'/'. $this->uploadDir().'/'.$this->model->id;
    }

    public function uploadDir(){
        return lcfirst(Inflector::pluralize(StringHelper::basename($this->model->className())));
    }

    public function generateFileName($file){
        return uniqid() . '.' . $file->extension;
    }
}

?>
