<?php

namespace wardany\dform\helpers;
use Yii;
use Imagine\Image\ManipulatorInterface;
use yii\base\InvalidParamException;
use yii\db\BaseActiveRecord;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * Description of FieldHelper
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */

class ImageHelper extends UploadHelper{
    /**
     * @throws \yii\base\InvalidParamException
     */
    public function createThumbs()
    {
        foreach ($this->attributes as $attribute) {
            $path = $this->getUploadPath($attribute);
            foreach ($this->thumbs as $profile => $config) {
                $thumbPath = $this->getThumbUploadPath($attribute, $profile);
                if ($thumbPath !== null) {
                    if (!FileHelper::createDirectory(dirname($thumbPath))) {
                        throw new InvalidParamException("Directory specified in 'thumbPath' attribute doesn't exist or cannot be created.");
                    }
                    if (!is_file($thumbPath) && is_file($path)) {
                        $this->generateImageThumb($config, $path, $thumbPath);
                    }
                }
            }
        }
    }

    /**
     * @param string $attribute
     * @param string $profile
     * @param bool $old
     * @return string
     */
    public function getThumbUploadPath($attribute, $profile = 'thumb', $old = false)
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        $path = $this->resolvePath($this->thumbPath);
        $attribute = ($old === true) ? $model->getOldAttribute($attribute) : $model->$attribute;
        $filename = $this->getThumbFileName($attribute, $profile);

        return $filename ? Yii::getAlias($path . '/' . $filename) : null;
    }

    /**
     * @param string $attribute
     * @param string $profile
     * @return string|null
     */
    public function getThumbUploadUrl($attribute, $profile = 'thumb')
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        $path = $this->getUploadPath($attribute, true);
        if (is_file($path)) {
            if ($this->createThumbsOnRequest) {
                $this->createThumbs();
            }
            $url = $this->resolvePath($this->thumbUrl);
            $fileName = $model->getOldAttribute($attribute);
            $thumbName = $this->getThumbFileName($fileName, $profile);

            return Yii::getAlias($url . '/' . $thumbName);
        } elseif ($this->placeholder) {
            return $this->getPlaceholderUrl($profile);
        } else {
            return null;
        }
    }

    /**
     * @param $profile
     * @return string
     */
    protected function getPlaceholderUrl($profile)
    {
        list ($path, $url) = Yii::$app->assetManager->publish($this->placeholder);
        $filename = basename($path);
        $thumb = $this->getThumbFileName($filename, $profile);
        $thumbPath = dirname($path) . DIRECTORY_SEPARATOR . $thumb;
        $thumbUrl = dirname($url) . '/' . $thumb;

        if (!is_file($thumbPath)) {
            $this->generateImageThumb($this->thumbs[$profile], $path, $thumbPath);
        }

        return $thumbUrl;
    }

    /**
     * @inheritdoc
     */
    protected function delete($attribute, $old = false)
    {
        parent::delete($attribute, $old);

        $profiles = array_keys($this->thumbs);
        foreach ($profiles as $profile) {
            $path = $this->getThumbUploadPath($attribute, $profile, $old);
            if (is_file($path)) {
                unlink($path);
            }
        }
    }

    /**
     * @param $filename
     * @param string $profile
     * @return string
     */
    protected function getThumbFileName($filename, $profile = 'thumb')
    {
        return $profile . '-' . $filename;
    }

    /**
     * @param $config
     * @param $path
     * @param $thumbPath
     */
    protected function generateImageThumb($config, $path, $thumbPath)
    {
        $width = ArrayHelper::getValue($config, 'width');
        $height = ArrayHelper::getValue($config, 'height');
        $quality = ArrayHelper::getValue($config, 'quality', 100);
        $mode = ArrayHelper::getValue($config, 'mode', ManipulatorInterface::THUMBNAIL_INSET);

        $image = Image::getImagine()->open($path);
        $image_width = $image->getSize()->getWidth() ;
        $image_height = $image->getSize()->getHeight() ;

        if (!$width || !$height) {

            $ratio = $image_width / $image_height;
            if ($width) {
                $height = ceil($width / $ratio);
            } else {
                $width = ceil($height * $ratio);
            }
        }

        if($image_width < $width)
            $width = $image_width;
        if($image_height < $height)
        {
            $height = $image_height ;
        }
        // Fix error "PHP GD Allowed memory size exhausted".
        ini_set('memory_limit', '512M');
        Image::thumbnail($path, $width, $height, $mode)->save($thumbPath, ['quality' => $quality]);
    }
}
