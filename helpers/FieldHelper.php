<?php

namespace wardany\dform\helpers;

/**
 * Description of FieldHelper
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */

use wardany\dform\fields;
use Yii;

class FieldHelper {

    const TEXT_INPUT = 'textInput';
    const TEXT_AREA = 'textArea';
    const EMAIL = 'email';
    const URL = 'url';
    const NUMBER = 'number';
    const PHONE = 'phone';
    const CHECKBOX_LIST = 'checkboxList';
    const DROPDOWN_LIST = 'dropdownList';
    const RADIO_LIST = 'radioList';
    const FILE = 'file';
    const IMAGE = 'image';


    public static function fields() {
        return[
            'textInput'=>[
                'title'=> Yii::t('d_form', 'Text input'),
                'class'=> fields\FieldTextInput::className(),
                'icon'=>'glyphicon glyphicon-text-width',
                'form'=> '__textinput',
            ],
            'textArea'=>[
                'title'=> Yii::t('d_form', 'Textarea'),
                'class'=> fields\FieldTextarea::className(),
                'icon'=>'glyphicon glyphicon-edit',
                'form'=> '__textarea',
            ],
            'url'=>[
                'title'=> Yii::t('d_form', 'Url field'),
                'class'=> fields\FieldUrl::className(),
                'icon'=>'glyphicon glyphicon-link',
                'form'=> '__url',
            ],
            'email'=>[
                'title'=> Yii::t('d_form', 'Email field'),
                'class'=> fields\FieldEmail::className(),
                'icon'=>'glyphicon glyphicon-envelope',
                'form'=> '__email',
            ],
            'number'=>[
                'title'=> Yii::t('d_form', 'Number field'),
                'class'=> fields\FieldNumber::className(),
                'icon'=>'glyphicon glyphicon-shopping-cart',
                'form'=> '__number',
            ],
            'phone'=>[
                'title'=> Yii::t('d_form', 'Phone field'),
                'class'=> fields\FieldPhone::className(),
                'icon'=>'glyphicon glyphicon-earphone',
                'form'=> '__phone',
            ],
            'checkboxList'=>[
                'title'=> Yii::t('d_form', 'Checkbox List'),
                'class'=> fields\FieldCheckboxList::className(),
                'icon'=> 'glyphicon glyphicon-text-width',
                'form'=> '__checkboxlist',
            ],
            'radioList'=>[
                'title'=> Yii::t('d_form', 'Radio list'),
                'class'=> fields\FieldRadioList::className(),
                'icon'=>'glyphicon glyphicon-record',
                'form'=> '__radiolist',
            ],
            'dropdownList'=>[
                'title'=> Yii::t('d_form', 'Dropdown list'),
                'class'=> fields\FieldDropdownList::className(),
                'icon'=>'glyphicon glyphicon-collapse-down',
                'form'=> '__dropdownlist',
            ],
//            'file'=>[
//                'title'=> Yii::t('d_form', 'File field'),
//                'class'=> fields\FieldFile::className(),
//                'icon'=>'glyphicon glyphicon-folder-open',
//                'form'=> '__file',
//            ],
//            'image'=>[
//                'title'=> Yii::t('d_form', 'Image field'),
//                'class'=> fields\FieldImage::className(),
//                'icon'=>'glyphicon glyphicon-picture',
//                'form'=> '__image',
//            ],
        ];
    }

    public static function field($field) {
        if(key_exists($field, self::fields()))
            {return self::fields()[$field];}
        return null;
    }

}
