<?php

namespace wardany\dform;
use yii\base\InvalidConfigException;

class DynamicForm extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'wardany\dform\controllers';

    public $upload_path ;
    public $upload_url ;

    /**
     * string|array
     */
    public $insert_scenario = 'insert';

    /**
    * string|array
    */
    public $update_scenario = 'update';

    public function init(){
        parent::init();
        if (empty($this->upload_url)) {
            throw new InvalidConfigException('"upload_url" property must be set.');
        }
        if (empty($this->upload_path)) {
            throw new InvalidConfigException('"upload_path" property must be set.');
        }
    }
}
