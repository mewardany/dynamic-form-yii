<?php

namespace wardany\dform\fields;

use Yii;
use wardany\dform\models\Field;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "form_radio_list".
 *
 * @property integer $id
 * @property integer $required
 * @property string $options
 * @property string $class_name
 * @property string $label_attribute
 * @property string $value_attribute
 * @property string $items //
 *
 * @property FormField[] $formFields
 */
class FieldRadioList extends Field {
    const delimiter = '***!***';
    public function init() {
        $this->_field_options =[
            'required'  => false,
            'items'    => null,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['required'], 'integer'],
            [['items'], 'string'],
            ['items', 'required']

        ]);
    }


    public function scenarios() {
        return ArrayHelper::merge([
            'insert'=>array_merge(['required', 'items'], array_keys(self::htmlElements())),
            'update'=>array_merge(['required', 'items'], array_keys(self::htmlElements())),
        ],parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'required' => 'Required',
            'options' => 'Options',
            'class_name' => 'Class Name',
            'label_attribute' => 'Label Attribute',
            'value_attribute' => 'Value Attribute',
        ];
    }

    public function getInput($model = null, $form = null) {
        if($model && $form)
            return $form->field($model, $this->attribute_name, [
                'options'=> $this->getFormOptions(),
                'inputOptions'=> $this->getFormInputOptions(),
                'labelOptions'=> $this->getFormLabelOptions(),
                'errorOptions'=> $this->getFormErrorOptions(),
                'hintOptions'=> $this->getFormHintOptions(),
                ])
                ->radioList($this->getItemsAsArray())
                ->label($this->getHideLabel()? false:  $this->tLabel) ;


        $label = Html::label($this->attribute_label, 'field_'.$this->attribute_name, $this->getFormLabelOptions());
        $input = Html::radioList($this->attribute_name, null, $this->getItemsAsArray(), ArrayHelper::merge(['id'=> $this->attribute_name], $this->getFormInputOptions()));
        $label = !$this->getHideLabel()? $label: null;

        return Html::tag('div', $label. "\n". $input. "\n" , ArrayHelper::merge(['id'=> 'field-'.$this->attribute_name], $this->getFormOptions()));
    }

    public function getRules(){
        $rules = [];
        if($this->required)
            $rules[] = [$this->attribute_name, 'required'];

        $validator = [$this->attribute_name, 'in', 'range'=> array_keys($this->getItemsAsArray())];
        $rules[] = $validator;

        return $rules;
    }

    /*
     * getters and setters
     */

    public function getRequired(){
        return $this->_field_options['required'];
    }

    public function setRequired($value) {
        $this->_field_options['required'] = $value;
    }

    public function getItems(){
        $items = Json::decode($this->_field_options['items']);
        return is_array($items)?implode("\n", $items):null;
        return str_replace(self::delimiter, "\n", $value);
    }

    public function getItemsAsArray(){
        $items = Json::decode($this->_field_options['items']);
        $items_as_array =[];
        foreach ($items as $item) {
          $items_as_array[$item] = $this->getTItem($item);
        }
        return $items_as_array;
    }

    public function setItems($value) {
        $value = str_replace(array("\r\n", "\n", "\r"), self::delimiter,$value);
        $value = explode(self::delimiter, $value);
        $this->translateItems($value);
        $this->_field_options['items'] = Json::encode($value);
    }

}
