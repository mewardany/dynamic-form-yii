<?php

namespace wardany\dform\fields;

use wardany\dform\helpers\ValidationCreator;
use wardany\dform\models\Field;
use yii\bootstrap\ActiveField;
use yii\helpers\Html;
use wardany\dform\widgets\DynamicFormActiveField;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "form_text_input".
 *
 * @property integer $id
 * @property integer $required
 * @property integer $length
 * @property integer $max
 * @property integer $min
 * @property string $place_holder //
 *
 * @property Field[] $field
 */

class FieldTextInput extends Field {

    public function init() {
        $this->_field_options =[
            'required'  => false,
            'max'       => 255,
            'min'       => null,
            'length'    => null,
        ];
        $this->_form_options = ArrayHelper::merge($this->_form_options,[
            'place_holder' => '',
            'right_icon'=>[
                'show'=> false,
                'text'=> null,
                'icon'=> null,
                'class'=> null
            ],
            'left_icon'=>[
                'show'=> false,
                'text'=> null,
                'icon'=> null,
                'class'=> null
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            ['max', 'required'],
            [['required', 'length', 'max', 'min'], 'integer'],
            ['required', 'boolean'],
            ['required', 'default', 'value'=> true],
            ['max', 'default', 'value'=> 255],
            [['min', 'length'], 'default', 'value'=> false],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'required' => 'Required',
            'length' => 'Length',
            'max' => 'Max',
            'min' => 'Min',
            'right_icon' => 'Right icon',
            'right_icon_text' => 'Right icon text',
            'left_icon' => 'Left icon',
            'left_icon_text' => 'Left icon text',

        ];
    }

    public function scenarios() {
        return ArrayHelper::merge([
            'insert'=>array_merge(['required', 'length', 'max', 'min'], array_keys(self::htmlElements())),
            'update'=>array_merge(['required', 'length', 'max', 'min'], array_keys(self::htmlElements())),
        ],parent::scenarios());
    }

    public function getRules(){
        return ValidationCreator::addRules($this->attribute_name)->re;
    }

    public function getInput($model = null, $form = null) {
        $icon = false ; $right_icon = null; $left_icon = null;

        if($this->showRightIcon){
                $right_icon = Html::tag('span', '<i class="glyphicon '.$this->rightIcon.'"></i> '.$this->rightIconText, ['class'=> 'input-group-addon '.$this->rightIconClass]);
                $icon = true;
        }
        if($this->showLeftIcon){
                $left_icon = Html::tag('span', $this->leftIconText. ' <i class="glyphicon '.$this->leftIcon.'"></i>', ['class'=> 'input-group-addon '.$this->leftIconClass]);
                $icon = true;
        }

        if($model && $form){
            $template = "{label}\n{input}\n{hint}\n{error}";
            if($icon)
                $template = "{label}\n<div class='input-group'>{$left_icon}\n{input}\n{$right_icon}</div>\n{hint}\n{error}";
            return $form->field($model, $this->attribute_name, [
                    'options'=> $this->getFormOptions(),
                    'template'=> $template,
                    'inputOptions'=> $this->getFormInputOptions(),
                    'labelOptions'=> $this->getFormLabelOptions(),
                    'errorOptions'=> $this->getFormErrorOptions(),
                    'hintOptions'=> $this->getFormHintOptions(),
                    ])
                    ->textInput()
                    ->label($this->getHideLabel()? false:  $this->tLabel) ;
                    // ->label($this->getHideLabel()? false: $this->attribute_label) ;
        }

        $label = Html::label($this->attribute_label, 'field_'.$this->attribute_name, $this->getFormLabelOptions());
        $input = Html::textInput($this->attribute_name, null, ArrayHelper::merge(['id'=> $this->attribute_name], $this->getFormInputOptions()));
        if($icon)
            $input = Html::tag('div', $left_icon. $input. $right_icon,['class'=> 'input-group']);
        $label = !$this->getHideLabel()? $label: null;

        return Html::tag('div', $label. "\n". $input. "\n" , ArrayHelper::merge(['id'=> 'field-'.$this->attribute_name], $this->getFormOptions()));
    }

    public static function htmlElements($element = null){
        $elements =   ArrayHelper::merge(parent::htmlElements(),[
            'placeHolder'           => 'Place holder',
            'showRightIcon'         => 'Show right icon',
            'rightIcon'             => 'Icon',
            'rightIconText'         => 'Text',
            'rightIconClass'        => 'Class',
            'showLeftIcon'          => 'Show right icon',
            'leftIcon'              => 'Icon',
            'leftIconText'          => 'Text',
            'leftIconClass'         => 'Class',
        ]);
        if($element === null)
            return $elements;
        else
            return $elements[$element];
    }

    /*
     * getters and setters
     */

    public function getRequired(){
        return $this->_field_options['required'];
    }

    public function setRequired($value) {
        $this->_field_options['required'] = $value;
    }

    public function getMax(){
        return $this->_field_options['max'];
    }

    public function setMax($value) {
        $this->_field_options['max'] = $value;
    }

    public function getMin(){
        return $this->_field_options['min'];
    }

    public function setMin($value) {
        $this->_field_options['min'] = $value;
    }

    public function getLength(){
        return $this->_field_options['length'];
    }

    public function setLength($value) {
        $this->_field_options['length'] = $value;
    }
}
