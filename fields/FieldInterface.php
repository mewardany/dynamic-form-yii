<?php

namespace wardany\dform\fields;

/**
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */
interface FieldInterface {
    /**
     * return string
     */
    public function getInput();
    
    public static function getBaseInput($model);
}
