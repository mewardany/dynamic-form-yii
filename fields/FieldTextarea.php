<?php

namespace wardany\dform\fields;

use Yii;
use dosamigos\ckeditor\CKEditor;
use dosamigos\ckeditor\CKEditorInline;
use wardany\dform\behaviors\DynamicFormField;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "form_text_area".
 *
 * @property integer $id
 * @property integer $required
 * @property integer $editorType
 * @property integer $max
 * @property integer $min
 *
 * @property FormField[] $formFields
 */
class FieldTextarea extends \wardany\dform\models\Field
{
    const TEXTAREA_DEFAULT = 1;
    const TEXTAREA_BASIC = 2;
    const TEXTAREA_STANDARD = 3;
    const TEXTAREA_FULL = 4;

    public function init() {
        $this->_field_options =[
            'required'  => false,
            'max'       => 255,
            'min'       => null,
        ];
        $this->_form_options = ArrayHelper::merge($this->_form_options,[
            'editor_type'  => self::TEXTAREA_DEFAULT,
            'place_holder'=> null
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['required', 'editorType', 'max', 'min'], 'integer'],
            ['editorType', 'in', 'range'=>  array_keys(self::textareaEditorTypes())]
        ]);
    }

    public function scenarios() {
        return ArrayHelper::merge(parent::scenarios(), [
            'insert'=> array_merge(['max', 'required', 'editorType', 'max', 'min', 'attribute_label', 'attribute_name'], array_keys(self::htmlElements())),
            'update'=> array_merge(['max', 'required', 'editorType', 'max', 'min', 'attribute_label'], array_keys(self::htmlElements())),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'required' => 'Required',
            'editorType' => 'Editor type',
            'max' => 'Max',
            'min' => 'Min',
        ];
    }

    public static function textareaEditorTypes(){
      return [
        self::TEXTAREA_DEFAULT  => 'Default',
        self::TEXTAREA_BASIC    => 'Basic',
        self::TEXTAREA_STANDARD => 'Standard',
        self::TEXTAREA_FULL     => 'Full'
      ];
    }


    public function getRules(){

        if($this->getOption('required'))
            $rules[] = [$this->attribute_name, 'required'];

        $string_validator = [$this->attribute_name, 'string', 'max'=> $this->max];
        if(is_int($this->getOption('min')))
            $string_validator['min'] = $this->getOption('min');
        $rules[] = $string_validator;

        return $rules;
    }

    public function getInput($model = null, $form = null) {
        switch ($this->editorType) {
            case self::TEXTAREA_BASIC :
                return $this->ckeditor('basic', $model, $form);
                break;
            case self::TEXTAREA_STANDARD :
                return $this->ckeditor('standard', $model, $form);
                break;
            case self::TEXTAREA_FULL :
                return $this->ckeditor('full', $model, $form);
                break;
            default:
                return $this->defaultEditor($model, $form);
                break;
        }
    }

    public function defaultEditor($model= null, $form= null) {
        if($model && $form)
            return $form->field($model, $this->attribute_name, [
                'options'=> $this->getFormOptions(),
                'inputOptions'=> $this->getFormInputOptions(),
                'labelOptions'=> $this->getFormLabelOptions(),
                'errorOptions'=> $this->getFormErrorOptions(),
                'hintOptions'=> $this->getFormHintOptions(),
                ])
                ->textarea()
                ->label($this->getHideLabel()? false: $this->attribute_label) ;


        $label = Html::label($this->attribute_label, 'field_'.$this->attribute_name, $this->getFormLabelOptions());
        $input = Html::textarea($this->attribute_name, null, ArrayHelper::merge(['id'=> $this->attribute_name], $this->getFormInputOptions()));
        $label = !$this->getHideLabel()? $label: null;

        return Html::tag('div', $label. "\n". $input. "\n" , ArrayHelper::merge(['id'=> 'field-'.$this->attribute_name], $this->getFormOptions()));
    }

    public function ckeditor($preset, $model= null, $form= null) {
        if($model && $form)
            return $form->field($model, $this->attribute_name)->widget(CKEditor::className(), [
                'options'=> $this->getFormOptions(),
                // 'inputOptions'=> $this->getFormInputOptions(),
                // 'labelOptions'=> $this->getFormLabelOptions(),
                // 'errorOptions'=> $this->getFormErrorOptions(),
                // 'hintOptions'=> $this->getFormHintOptions(),
                'preset' => $preset
            ]);
        else
            return CKEditor::widget(['name'=> $this->attribute_name, 'preset' => $preset]);
    }

    public static function htmlElements($element = null){
        $elements =   ArrayHelper::merge([
            'placeHolder'           => 'Place holder',
        ], parent::htmlElements());
        if($element === null)
            return $elements;
        else
            return $elements[$element];
    }

     /*
     * getters and setters
     */

    public function getRequired(){
        return $this->_field_options['required'];
    }

    public function setRequired($value) {
        $this->_field_options['required'] = $value;
    }

    public function getMax(){
        return $this->_field_options['max'];
    }

    public function setMax($value) {
        $this->_field_options['max'] = $value;
    }

    public function getMin(){
        return $this->_field_options['min'];
    }

    public function setMin($value) {
        $this->_field_options['min'] = $value;
    }

    public function getEditorType(){
        return $this->_form_options['editor_type'];
    }

    public function setEditorType($value) {
        $this->_form_options['editor_type'] = $value;
    }

}
