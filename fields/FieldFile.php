<?php

namespace wardany\dform\fields;

use Yii;

/**
 * This is the model class for table "form_file".
 *
 * @property integer $id
 * @property integer $required
 * @property string $extensions
 * @property string $mime_types
 * @property integer $min_size
 * @property integer $max_size
 * @property integer $max_files
 *
 * @property FormField[] $formFields
 */
class FieldFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['required', 'min_size', 'max_size', 'max_files'], 'integer'],
            [['extensions'], 'string'],
            [['mime_types'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'required' => 'Required',
            'extensions' => 'Extensions',
            'mime_types' => 'Mime Types',
            'min_size' => 'Min Size',
            'max_size' => 'Max Size',
            'max_files' => 'Max Files',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormFields()
    {
        return $this->hasMany(FormField::className(), ['field_id' => 'id']);
    }
}
