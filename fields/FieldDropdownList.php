<?php

namespace wardany\dform\fields;

use Yii;
use wardany\dform\models\Field;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/**
 * This is the model class for table "form_dropdown_list".
 *
 * @property integer $id
 * @property integer $required
 * @property string $prompt
 * @property string $options
 * @property string $class_name
 * @property string $label_attribute
 * @property string $value_attribute
 *
 * @property FormField[] $formFields
 */
class FieldDropdownList extends Field
{
    const ITEMS_TYPE_NUMBER = 1;
    const ITEMS_TYPE_YEAR = 2;
    const ITEMS_TYPE_MONTH = 3;
    const ITEMS_TYPE_DAY = 4;

    const delimiter = '***!***';

    public function init() {
        $this->_field_options =[
            'required' => false,
            'items' => null,
            'startValue'=> null,
            'endValue' => null,
            'endValueType' => null,
            'startValueType' => null,
        ];
        $this->_form_options = ArrayHelper::merge($this->_form_options,[
            'prompt' => ''
        ]);
    }

    /**
     * @inheritdoc
     */
     public function rules()
     {
         return ArrayHelper::merge(parent::rules(),[
             [['required', 'startValue', 'endValue'], 'integer'],
             ['items', 'string'],
             ['endValueType', 'in', 'range'=> array_keys($this->itemTypes())],
             ['startValueType', 'in', 'range'=> array_keys($this->itemTypes())],
             ['prompt', 'string', 'max'=> 61]
         ]);
     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'required' => 'Required',
            'prompt' => 'Prompt',
            'options' => 'Options',
            'class_name' => 'Class Name',
            'label_attribute' => 'Label Attribute',
            'value_attribute' => 'Value Attribute',
        ];
    }

    public function scenarios() {
        return ArrayHelper::merge([
            'insert'=>array_merge(['required', 'items', 'startValue', 'endValue', 'endValueType', 'startValueType'], array_keys(self::htmlElements())),
            'update'=>array_merge(['required', 'items', 'startValue', 'endValue', 'endValueType', 'startValueType'], array_keys(self::htmlElements())),
        ],parent::scenarios());
    }


    public function getInput($model = null, $form = null) {
        if($model && $form)
            return $form->field($model, $this->attribute_name, [
                'options'=> $this->getFormOptions(),
                'inputOptions'=> $this->getFormInputOptions(),
                'labelOptions'=> $this->getFormLabelOptions(),
                'errorOptions'=> $this->getFormErrorOptions(),
                'hintOptions'=> $this->getFormHintOptions(),
                ])
                ->dropdownList($this->getItemsAsArray(), ['prompt' => $this->getPrompt()])
                ->label($this->getHideLabel()? false:  $this->tLabel) ;
        $options = ['id'=> $this->attribute_name];
        if(!empty($this->prompt))
            $options['prompt']= $this->prompt;
        $label = Html::label($this->attribute_label, 'field_'.$this->attribute_name, $this->getFormLabelOptions());
        $input = Html::dropdownList($this->attribute_name, null, $this->getItemsAsArray(), ArrayHelper::merge($options, $this->getFormInputOptions()));
        $label = !$this->getHideLabel()? $label: null;

        return Html::tag('div', $label. "\n". $input. "\n" , ArrayHelper::merge(['id'=> 'field-'.$this->attribute_name], $this->getFormOptions()));
    }

        public function getRules(){
            $rules = [];
            if($this->required)
                $rules[] = [$this->attribute_name, 'required'];

            $validator = [$this->attribute_name, 'in', 'range'=> array_keys($this->getItemsAsArray())];
            $rules[] = $validator;

            return $rules;
        }


        public static function htmlElements($element = null){
            $elements =   ArrayHelper::merge(parent::htmlElements(),[
                'prompt'           => 'Prompt',
            ]);
            if($element === null)
                return $elements;
            else
                return $elements[$element];
        }

        /*
         * getters and setters
         */

        public function getRequired(){
            return $this->_field_options['required'];
        }

        public function setRequired($value) {
            $this->_field_options['required'] = $value;
        }

        public function getStartValue(){
            if(key_exists('startValue', $this->_field_options))
                return $this->_field_options['startValue'];
        }

        public function setStartValue($value) {
            if(key_exists('startValue', $this->_field_options))
                $this->_field_options['startValue'] = $value;
        }

        public function getEndValueType(){
            if(key_exists('endValueType', $this->_field_options))
                return $this->_field_options['endValueType'];
        }

        public function setEndValueType($value) {
            if(key_exists('endValueType', $this->_field_options))
                $this->_field_options['endValueType'] = $value;
        }

        public function getStartValueType(){
            if(key_exists('startValueType', $this->_field_options))
                return $this->_field_options['startValueType'];
        }

        public function setStartValueType($value) {
            if(key_exists('startValueType', $this->_field_options))
                $this->_field_options['startValueType'] = $value;
        }

        public function getEndValue(){
            if(key_exists('endValue', $this->_field_options))
                return $this->_field_options['endValue'];
        }

        public function setEndValue($value) {
            if(key_exists('endValue', $this->_field_options))
                $this->_field_options['endValue'] = $value;
        }

        public function getItems(){
            if(!empty($this->_field_options['items'])){
                $items = Json::decode($this->_field_options['items']);
                return is_array($items)?implode("\n", $items):null;
            }
            return null;
        }


        public function setItems($value) {
            if(empty($value)){
                $this->_field_options['items'] = null;
            }
            else {
                $value = str_replace(array("\r\n", "\n", "\r"), self::delimiter,$value);
                $value = explode(self::delimiter, $value);
                $this->_field_options['items'] = Json::encode($value);
            }

        }

        public function getItemsAsArray(){
            if(!empty($this->items)){
                $items = Json::decode($this->_field_options['items']);
                $items_as_array =[];
                foreach ($items as $item) {
                  $items_as_array[$item] = $this->getTItem($item);
                }
                return $items_as_array;
            }
            return $this->getRangeItems();
        }


        public function getRangeItems() {
            $from= 0 ; $to = 0;

            switch ($this->getEndValueType()) {
                case self::ITEMS_TYPE_NUMBER :
                    $to = $this->endValue;
                    break;
                case self::ITEMS_TYPE_YEAR :
                    $to = date('Y');
                    break;
                case self::ITEMS_TYPE_MONTH :
                    $to = date('m');
                    break;
                case self::ITEMS_TYPE_DAY :
                    $to = date('d');
                    break;
            }

            switch ($this->getStartValueType()) {
                case self::ITEMS_TYPE_NUMBER :
                    $from = $this->endValue;
                    break;
                case self::ITEMS_TYPE_YEAR :
                    $from = date('Y');
                    break;
                case self::ITEMS_TYPE_MONTH :
                    $from = date('m');
                    break;
                case self::ITEMS_TYPE_DAY :
                    $from = date('d');
                    break;
            }
            return array_combine(range($from, $to),range($from, $to));
        }

      public static function itemTypes()
      {
        return [
          self::ITEMS_TYPE_NUMBER => 'Number',
          self::ITEMS_TYPE_YEAR => 'Current year',
          self::ITEMS_TYPE_MONTH => 'Current month',
          self::ITEMS_TYPE_DAY => 'Current day',
        ];
      }
}
