<?php

namespace wardany\dform\fields;

use Yii;
use Imagine\Image\ImageInterface;
use wardany\dform\helpers\UploadHelper;
use wardany\dform\models\Field;
use wardany\uploader\UploadImageBehavior;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "form_image".
 *
 * @property integer $id
 * @property integer $required
 * @property string $extensions
 * @property integer $minSize
 * @property integer $maxSize
 * @property integer $minWidth
 * @property integer $maxWidth
 * @property integer $minHeight
 * @property integer $maxHeight
 * @property integer $maxFiles
 * @property string $placeHolder
 * @property string $profiles
 *
 * @property FormField[] $formFields
 */
class FieldImage extends Field
{

    public function init() {
        $this->_field_options = [
            'required' => false,
            'extensions' => null,
            'minSize' => null,
            'maxSize' => null,
            'minWidth' => null,
            'maxWidth' => null,
            'minHeight' => null,
            'maxHeight' => null,
            'maxFiles' => 1,
            'profiles' =>[
                'thumb'=> ['width'=> 100, 'height'=> 100, 'quality'=> 90, 'mod'=> ImageInterface::THUMBNAIL_OUTBOUND]
            ]
        ];
        $this->_form_options = ArrayHelper::merge($this->_form_options,[
            'place_holder' => null,
            'parent_node_id'=> null,
            'order'=> 0,
            'hide_label' => false,
            'options'=>['class' => 'form-group'],
            'inputOptions'=> ['class' => 'form-control'],
            'errorOptions'=> ['class' => 'help-block'],
            'labelOptions'=> [
                'class' => 'control-label'
            ],
            'hintOptions'=> ['class' => 'hint-block']
        ]);
    }

    /**
    * @inheritdoc
    */
    public function rules() {
        return array_merge(parent::rules(),[
            [['required', 'minSize', 'maxSize', 'minWidth', 'maxWidth', 'minHeight', 'maxHeight'], 'integer'],
            [['extensions'], 'string', 'max'=>255],
            [['maxFiles'], 'required'],
            ['maxFiles', 'integer', 'min'=> 1],
            ['profiles', 'string'],
            ['placeHolder', 'image', 'skipOnEmpty'=> true, 'on'=>['insert', 'update']],
        ]);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'required' => 'Required',
            'extensions' => 'Extensions',
            'minSize' => 'Min Size',
            'maxSize' => 'Max Size',
            'minWidth' => 'Min Width',
            'maxWidth' => 'Max Width',
            'minHeight' => 'Min Height',
            'maxHeight' => 'Max Height',
            'maxFiles' => 'Max Files',
            'placeHolder' => 'PlaceHolder',
        ];
    }
    public function scenarios() {
        return ArrayHelper::merge([
            'insert'=> array_merge(['required', 'minSize', 'maxSize', 'minWidth', 'maxWidth', 'minHeight', 'maxHeight', 'maxFiles', 'extensions', 'profiles', 'placeHolder'], array_keys(self::htmlElements())),
            'update'=> array_merge(['required', 'minSize', 'maxSize', 'minWidth', 'maxWidth', 'minHeight', 'maxHeight', 'maxFiles', 'extensions', 'profiles', 'placeHolder'], array_keys(self::htmlElements())),
        ],parent::scenarios());
    }

    public function behaviors() {
        $uploadHelper = new UploadHelper($this);
        return ArrayHelper::merge(parent::behaviors(),[
            [
                'class'=> UploadImageBehavior::className(),
                'attributes'=> ['placeHolder'],
                'scenarios'=> ['updater', 'insert'],
                'path' => $uploadHelper->getUploadPath(),
                'url' => $uploadHelper->getUploadUrl(),
                'thumbs'=>[
                    'thumb'=>['width'=> 100, 'height'=> 160]
                ]
            ]
        ]);
    }

    public function getRules(){
        $image_validator = [$this->attribute_name, 'image'];

        if($this->maxFiles > 1)
            $image_validator['maxFiles'] = $this->maxFiles;
        if(!empty($this->maxHeight))
            $image_validator['maxHeight'] = $this->maxHeight;
        if($this->minHeight)
            $image_validator['minHeight'] = $this->minHeight;
        if($this->maxWidth)
            $image_validator['maxWidth'] = $this->maxWidth;
        if($this->minWidth)
            $image_validator['minWidth'] = $this->minWidth;
        if($this->maxSize)
            $image_validator['maxSize'] = $this->maxSize;
        if($this->minSize)
            $image_validator['minSize'] = $this->minSize;
        if($this->extensions)
            $image_validator['extensions'] = $this->extensions;

        if($this->required){
            $image_validator['skipOnEmpty'] = false;
            $image_validator['on']= Yii::$app->controller->module->insert_scenario;
            $rules[] = $image_validator;
            $image_validator['skipOnEmpty'] = true;
            $image_validator['on']= Yii::$app->controller->module->update_scenario;
            $rules[] = $image_validator;
        }
        else{
            $scenarios = [];
            if(is_array(Yii::$app->controller->module->insert_scenario))
                $scenarios = Yii::$app->controller->module->insert_scenario ;
            else
                $scenarios[] = Yii::$app->controller->module->insert_scenario;
            if(is_array(Yii::$app->controller->module->update_scenario))
                $scenarios = ArrayHelper::merge($scenarios, Yii::$app->controller->module->update_scenario) ;
            else
                $scenarios[] = Yii::$app->controller->module->update_scenario;

            $image_validator['skipOnEmpty'] = true;
            $image_validator['on']= $scenarios;
            $rules[] = $image_validator;
        }
        return $rules;
    }

    /**
     * [getInput description]
     * @param  ِActiveRecord $model
     * @param  ActiveForm $form
     */
    public function getInput($model = null, $form = null) {
        $inputOptions = ArrayHelper::merge($this->getFormInputOptions(), ['accept'=>'image/*']);
        if($this->getMaxFiles() > 1)
            $inputOptions['multiple']= "true";
        if($model && $form){
            return $form->field($model, $this->attribute_name.'[]', [
                    'options'=> $this->getFormOptions(),
                    'inputOptions'=> $inputOptions,
                    'labelOptions'=> $this->getFormLabelOptions(),
                    'errorOptions'=> $this->getFormErrorOptions(),
                    'hintOptions'=> $this->getFormHintOptions(),
                    ])
                    ->fileInput()
                    ->label($this->getHideLabel()? false: $this->attribute_label) ;
        }

        $label = !$this->getHideLabel()? Html::label($this->attribute_label, 'field_'.$this->attribute_name, $this->getFormLabelOptions()): null;
        $input = Html::fileInput($this->attribute_name, null, ArrayHelper::merge(['id'=> $this->attribute_name], $inputOptions));

        return Html::tag('div', $this->getOverlayButtons(). "\n". $label. "\n". $input. "\n" , ArrayHelper::merge(['id'=> 'field-'.$this->attribute_name], $this->getFormOptions()));
    }

    public function getRequired(){
        return $this->_field_options['required'];
    }

    public function setRequired($value) {
        $this->_field_options['required'] = $value;
    }

    public function getMaxSize(){
        return $this->_field_options['maxSize'];
    }

    public function setMaxSize($value) {
        $this->_field_options['maxSize'] = $value;
    }

    public function getMinSize(){
        return $this->_field_options['minSize'];
    }

    public function setMinSize($value) {
        $this->_field_options['minSize'] = $value;
    }

    public function getMaxHeight(){
        return $this->_field_options['maxHeight'];
    }

    public function setMaxHeight($value){
        $this->_field_options['maxHeight'] = $value;
    }

    public function getMinHeight(){
        return $this->_field_options['minHeight'];
    }

    public function setMinHeight($value) {
        $this->_field_options['minHeight'] = $value;
    }

    public function getMaxWidth(){
        return $this->_field_options['maxWidth'];
    }

    public function setMaxWidth($value){
        $this->_field_options['maxWidth'] = $value;
    }

    public function getMinWidth(){
        return $this->_field_options['minWidth'];
    }

    public function setMinWidth($value) {
        $this->_field_options['minWidth'] = $value;
    }

    public function getMaxFiles(){
        return $this->_field_options['maxFiles'];
    }

    public function setMaxFiles($value){
        $this->_field_options['maxFiles'] = $value;
    }

    public function getExtensions(){
        return $this->_field_options['extensions'];
    }

    public function setExtensions($value){
        $this->_field_options['extensions'] = $value;
    }
}
