<?php
namespace wardany\dform\widgets;

use Yii;
use wardany;
use wardany\dform\assets\FormAsset;
use wardany\dform\helpers\FieldHelper;
use yii\bootstrap\Widget;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\helpers\Html;
use wardany\dform\models\Field;

 /**
 * @author muhammad wardany
 */
class Titles extends Widget
{
    /**
     * @var wardany\dform\models\Field
     */
    public $model ;

    public $parent_node_id = null ;

    public $draggable = false ;

    public $controller = null;

    public function run()
    {
        parent::run();

        FormAsset::register($this->getView());

        $fields = Field::find()
                ->where(['form_id'=> $this->model->id])
                ->orderBy(new \yii\db\Expression('JSON_EXTRACT(form_options, "$.order") ASC'));

        if($this->parent_node_id !== null)
                $fields->andWhere(new \yii\db\Expression('JSON_EXTRACT(form_options, "$.parent_node_id") = :parent_node_id'))->params([':parent_node_id'=> $this->parent_node_id]);

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $fields,
        ]);
        Pjax::begin([
            'id' => 'inputs-list',
            'timeout' => 5000,
            'enablePushState' => true
        ]);
        echo ListView::widget([
            'emptyText' =>false,
            'dataProvider' => $dataProvider,
            'itemOptions'=>function ($model, $key, $index, $widget){
                return [
                    'class'=>'sortable-field',
                    'data-id'=> $model->id,
                    'id'=> $model->attribute_name,
                ];
            },
            'options'=>[
                'class'=> 'fields-list',
                'id'=> 'sortable',
                'sort-url'=> Url::to([$this->controller.'sort-fields'])
            ],
            'layout'=>"{items}",
            'itemView' => function ($model, $key, $index, $widget) {
                $input = $model->getInput();
                $overlay_buttons = $this->getOverlayButtons($model);

                return $overlay_buttons.$input ;
            }
        ]);
        Pjax::end();
    }

    public function getOverlayButtons($model) {
        return  Html::tag('span',
                    Html::a('<span class="glyphicon glyphicon-pencil"></span>',null,[
                            'class'=> 'btn btn-success btn-xs get-field',
                            'data-href'=> Url::to([$this->controller. 'edit-field','id'=> $model->id]),
                            'title'=>  FieldHelper::field($model->field_type)['title'],
                    ]).
                    " ".
                    Html::a('<span class="glyphicon glyphicon-trash"></span>',['remove-field', 'id'=>$model->id], [
                        'data-href'=> Url::to([$this->controller.'remove-field', 'id'=>$model->id]),
                        'class'=> 'btn btn-danger btn-xs remove-field',
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        // 'data-pjax' => '#inputs-list',
                    ]),
    //                Html::button('<span class="glyphicon glyphicon-move"></span>',[
    //                    'class'=> 'btn btn-default btn-xs drag-field',
    //                    'title' => Yii::t('yii', 'Drag field'),
    //                    'aria-label' => Yii::t('yii', 'Drag field'),
    //                    'ondragstart'=> "dragStart(event)",
    //                    'ondrag'=> "dragging(event)",
    //                    'draggable'=> true
    //                ]);
                    ['class'=> 'overlay_settings']
            );

    }


}
?>
