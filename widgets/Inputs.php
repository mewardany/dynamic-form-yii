<?php
namespace wardany\dform\widgets;

use wardany\dform\helpers\ActiveDynamicField;
use wardany\dform\models\Form;
use wardany\dform\models\Field;
use yii\bootstrap\Widget;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

 /**
 * @author muhammad wardany
 */
class Inputs extends Widget
{
    /**
     * @var Form the form class that hold all fields.
     */
    public $form_class;

    public $model ;

    /**
     * @var ActiveForm the form that this field is associated with.
     */
    public $form;

    public $parent_node_id = null ;

    public function run()
    {
        // $fields = Field::find()->where(['form_id'=> $this->model->form_id]);
        $fields = $this->model->getRelatedFields();
        if($fields instanceof Query){
            // if($this->parent_node_id !== null)
            //     $fields->andWhere(new \yii\db\Expression('JSON_EXTRACT(form_options, "$.parent_node_id") = :parent_node_id'))->params([':parent_node_id'=> $this->parent_node_id]);
        $fields->orderBy(new \yii\db\Expression('JSON_EXTRACT(form_options, "$.order") ASC'));

            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $fields,
            ]);
            echo ListView::widget([
                'emptyText' =>false,
                'dataProvider' => $dataProvider,
                'options'=>['tag'=> false],
                'layout'=>"{items}",
                'itemView' => function ($model, $key, $index, $widget) {
                    return $model->getInput($this->model, $this->form);
                }
            ]);

        }
    }
}
?>
