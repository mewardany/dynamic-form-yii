<?php
namespace wardany\dform\widgets;
use yii\bootstrap\InputWidget;
use wardany\dform\assets\IconPicker as IconPickerAsset;
use yii\helpers\Html;
use yii\web\View;


 /**
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */
class IconPicker extends InputWidget
{
    public function run(){
        IconPickerAsset::register($this->getView());
        $this->getView()->registerJs("jQuery(document).ready(function($) {
                $('#mytext').fontIconPicker({
                    source: fnt_icons_categorized,
                    emptyIcon: false,
                    hasSearch: false
                });
            });
        ",View::POS_END);
        return Html::activeTextInput($this->model, $this->attribute,['id' => "mytext"]);
    }
}
?>
